import {CAPTCHA} from '@/services/api';
import {request, METHOD} from '@/utils/request';

/**
 * 图片验证码请求
 * @param width 宽度
 * @param height 高度
 */
export async function captcha(width, height) {
        return request(CAPTCHA, METHOD.GET, {width: width, height: height});
}

export default {
        captcha
}