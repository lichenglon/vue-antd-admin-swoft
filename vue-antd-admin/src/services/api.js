//跨域代理前缀
// const API_PROXY_PREFIX='/api'
// const BASE_URL = process.env.NODE_ENV === 'production' ? process.env.VUE_APP_API_BASE_URL : API_PROXY_PREFIX
const BASE_URL = process.env.VUE_APP_API_BASE_URL
const BASE_SOCKET_URL = process.env.VUE_APP_SOCKET_BASE_URL
module.exports = {
        LOGIN: `${BASE_URL}/admin/login`,//后台登录
        CAPTCHA: `${BASE_URL}/common/captcha`,//图片验证码
        MENU: `${BASE_URL}/admin/menu`,//菜单
        USER: `${BASE_URL}/admin/user`,//用户
        ROLE: `${BASE_URL}/admin/role`,//角色
        UPLOAD_PIC: `${BASE_URL}/admin/uploadPic`,//上传图片
        ROUTES: `${BASE_URL}/common/routes`,//当前登录用户允许的路由权限
        PERSONAL_SETTING: `${BASE_URL}/common/personalSetting`,//个人设置
        MESSAGE: `${BASE_URL}/admin/message`,//消息通知
        SOCKET_MESSAGE: `${BASE_SOCKET_URL}/message`,//websocket消息通知

        GOODS: `${BASE_URL}/goods`,
        GOODS_COLUMNS: `${BASE_URL}/columns`,
}
