import {LOGIN, ROUTES} from '@/services/api'
import {request, METHOD, removeAuthorization} from '@/utils/request'

/**
 * 登录请求
 * @param username
 * @param password
 * @param captcha
 * @param key
 * @returns {Promise<*>}
 */
export async function login(username, password, captcha, key) {
        return request(LOGIN, METHOD.POST, {username: username, password: password, captcha: captcha, key: key});
}

export async function getRoutesConfig() {
        return request(ROUTES, METHOD.GET);
}

/**
 * 退出登录
 */
export function logout() {
        localStorage.removeItem(process.env.VUE_APP_ROUTES_KEY)
        localStorage.removeItem(process.env.VUE_APP_PERMISSIONS_KEY)
        localStorage.removeItem(process.env.VUE_APP_ROLES_KEY)
        removeAuthorization()
}

export default {
        login,
        logout,
        getRoutesConfig
}
