<?php declare(strict_types=1);


namespace App\Model;

use Swoft\Db\Annotation\Mapping\Column;
use Swoft\Db\Annotation\Mapping\Entity;
use Swoft\Db\Annotation\Mapping\Id;
use Swoft\Db\Eloquent\Model;


/**
 * 文件上传记录表
 * Class SystemUploadLog
 *
 * @since 2.0
 *
 * @Entity(table="system_upload_log")
 */
class SystemUploadLog extends Model
{
        use BaseModel;

        /**
         * 文件上传记录id
         * @Id(incrementing=false)
         * @Column()
         *
         * @var string
         */
        private $fileLogId;

        /**
         * 用户id
         *
         * @Column()
         *
         * @var string
         */
        private $userId;

        /**
         * 来源 1总后台上传
         *
         * @Column()
         *
         * @var int
         */
        private $source;

        /**
         * 类型 1图片 2文件 3视频
         *
         * @Column()
         *
         * @var int
         */
        private $type;

        /**
         * 文件大小 单位：MB
         *
         * @Column()
         *
         * @var float
         */
        private $size;

        /**
         * 客户端文件名
         *
         * @Column()
         *
         * @var string
         */
        private $clientFilename;

        /**
         * 文件MediaType类型
         *
         * @Column()
         *
         * @var string
         */
        private $mediaType;

        /**
         * 文件存储路径
         *
         * @Column()
         *
         * @var string
         */
        private $filePath;

        /**
         * 创建时间
         *
         * @Column()
         *
         * @var int
         */
        private $createTime;

        /**
         * 是否删除 0否 1是
         *
         * @Column()
         *
         * @var int
         */
        private $isDel;


        /**
         * @param string $fileLogId
         *
         * @return self
         */
        public function setFileLogId(string $fileLogId): self
        {
                $this->fileLogId = $fileLogId;

                return $this;
        }

        /**
         * @param string $userId
         *
         * @return self
         */
        public function setUserId(string $userId): self
        {
                $this->userId = $userId;

                return $this;
        }

        /**
         * @param int $source
         *
         * @return self
         */
        public function setSource(int $source): self
        {
                $this->source = $source;

                return $this;
        }

        /**
         * @param int $type
         *
         * @return self
         */
        public function setType(int $type): self
        {
                $this->type = $type;

                return $this;
        }

        /**
         * @param float $size
         *
         * @return self
         */
        public function setSize(float $size): self
        {
                $this->size = $size;

                return $this;
        }

        /**
         * @param string $clientFilename
         *
         * @return self
         */
        public function setClientFilename(string $clientFilename): self
        {
                $this->clientFilename = $clientFilename;

                return $this;
        }

        /**
         * @param string $mediaType
         *
         * @return self
         */
        public function setMediaType(string $mediaType): self
        {
                $this->mediaType = $mediaType;

                return $this;
        }

        /**
         * @param string $filePath
         *
         * @return self
         */
        public function setFilePath(string $filePath): self
        {
                $this->filePath = $filePath;

                return $this;
        }

        /**
         * @param int $createTime
         *
         * @return self
         */
        public function setCreateTime(int $createTime): self
        {
                $this->createTime = $createTime;

                return $this;
        }

        /**
         * @param int $isDel
         *
         * @return self
         */
        public function setIsDel(int $isDel): self
        {
                $this->isDel = $isDel;

                return $this;
        }

        /**
         * @return string
         */
        public function getFileLogId(): ?string

        {
                return $this->fileLogId;
        }

        /**
         * @return string
         */
        public function getUserId(): ?string

        {
                return $this->userId;
        }

        /**
         * @return int
         */
        public function getSource(): ?int

        {
                return $this->source;
        }

        /**
         * @return int
         */
        public function getType(): ?int

        {
                return $this->type;
        }

        /**
         * @return float
         */
        public function getSize(): ?float

        {
                return $this->size;
        }

        /**
         * @return string
         */
        public function getClientFilename(): ?string

        {
                return $this->clientFilename;
        }

        /**
         * @return string
         */
        public function getMediaType(): ?string

        {
                return $this->mediaType;
        }

        /**
         * @return string
         */
        public function getFilePath(): ?string

        {
                return $this->filePath;
        }

        /**
         * @return int
         */
        public function getCreateTime(): ?int

        {
                return $this->createTime;
        }

        /**
         * @return int
         */
        public function getIsDel(): ?int

        {
                return $this->isDel;
        }


}
