<?php declare(strict_types=1);


namespace App\Model;

use Swoft\Db\Annotation\Mapping\Column;
use Swoft\Db\Annotation\Mapping\Entity;
use Swoft\Db\Annotation\Mapping\Id;
use Swoft\Db\Eloquent\Model;


/**
 * 系统用户角色表
 * Class SystemUserRole
 *
 * @since 2.0
 *
 * @Entity(table="system_user_role")
 */
class SystemUserRole extends Model
{
        use BaseModel;

        /**
         *
         *
         * @Column()
         *
         * @var string
         */
        private $userId;

        /**
         * 角色id
         *
         * @Column()
         *
         * @var string
         */
        private $roleId;


        /**
         * @param string $userId
         *
         * @return self
         */
        public function setUserId(string $userId): self
        {
                $this->userId = $userId;

                return $this;
        }

        /**
         * @param string $roleId
         *
         * @return self
         */
        public function setRoleId(string $roleId): self
        {
                $this->roleId = $roleId;

                return $this;
        }

        /**
         * @return string
         */
        public function getUserId(): ?string

        {
                return $this->userId;
        }

        /**
         * @return string
         */
        public function getRoleId(): ?string

        {
                return $this->roleId;
        }


}
