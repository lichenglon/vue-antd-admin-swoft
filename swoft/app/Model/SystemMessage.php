<?php declare(strict_types=1);


namespace App\Model;

use Swoft\Db\Annotation\Mapping\Column;
use Swoft\Db\Annotation\Mapping\Entity;
use Swoft\Db\Annotation\Mapping\Id;
use Swoft\Db\Eloquent\Model;


/**
 * 系统消息通知待办表
 * Class SystemMessage
 *
 * @since 2.0
 *
 * @Entity(table="system_message")
 */
class SystemMessage extends Model
{
        use BaseModel;

        /**
         * 消息id
         * @Id(incrementing=false)
         * @Column()
         *
         * @var string
         */
        private $messageId;

        /**
         * 标题
         *
         * @Column()
         *
         * @var string
         */
        private $title;

        /**
         * 内容
         *
         * @Column()
         *
         * @var string
         */
        private $content;

        /**
         * 类型 1消息 2通知 3待办
         *
         * @Column()
         *
         * @var int
         */
        private $type;

        /**
         * 创建人
         *
         * @Column()
         *
         * @var string
         */
        private $userId;

        /**
         * 创建时间
         *
         * @Column()
         *
         * @var int
         */
        private $createTime;


        /**
         * @param string $messageId
         *
         * @return self
         */
        public function setMessageId(string $messageId): self
        {
                $this->messageId = $messageId;

                return $this;
        }

        /**
         * @param string $title
         *
         * @return self
         */
        public function setTitle(string $title): self
        {
                $this->title = $title;

                return $this;
        }

        /**
         * @param string $content
         *
         * @return self
         */
        public function setContent(string $content): self
        {
                $this->content = $content;

                return $this;
        }

        /**
         * @param int $type
         *
         * @return self
         */
        public function setType(int $type): self
        {
                $this->type = $type;

                return $this;
        }

        /**
         * @param string $userId
         *
         * @return self
         */
        public function setUserId(string $userId): self
        {
                $this->userId = $userId;

                return $this;
        }

        /**
         * @param int $createTime
         *
         * @return self
         */
        public function setCreateTime(int $createTime): self
        {
                $this->createTime = $createTime;

                return $this;
        }

        /**
         * @return string
         */
        public function getMessageId(): ?string

        {
                return $this->messageId;
        }

        /**
         * @return string
         */
        public function getTitle(): ?string

        {
                return $this->title;
        }

        /**
         * @return string
         */
        public function getContent(): ?string

        {
                return $this->content;
        }

        /**
         * @return int
         */
        public function getType(): ?int

        {
                return $this->type;
        }

        /**
         * @return string
         */
        public function getUserId(): ?string

        {
                return $this->userId;
        }

        /**
         * @return int
         */
        public function getCreateTime(): ?int

        {
                return $this->createTime;
        }


}
