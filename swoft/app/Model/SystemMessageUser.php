<?php declare(strict_types=1);


namespace App\Model;

use Swoft\Db\Annotation\Mapping\Column;
use Swoft\Db\Annotation\Mapping\Entity;
use Swoft\Db\Annotation\Mapping\Id;
use Swoft\Db\Eloquent\Model;


/**
 * 系统消息通知待办用户表
 * Class SystemMessageUser
 *
 * @since 2.0
 *
 * @Entity(table="system_message_user")
 */
class SystemMessageUser extends Model
{
        use BaseModel;

        /**
         * 消息id
         *
         * @Column()
         *
         * @var string
         */
        private $messageId;

        /**
         * 用户id
         *
         * @Column()
         *
         * @var string
         */
        private $userId;

        /**
         * 是否已读 1是 2否
         *
         * @Column()
         *
         * @var int
         */
        private $isRead;


        /**
         * @param string $messageId
         *
         * @return self
         */
        public function setMessageId(string $messageId): self
        {
                $this->messageId = $messageId;

                return $this;
        }

        /**
         * @param string $userId
         *
         * @return self
         */
        public function setUserId(string $userId): self
        {
                $this->userId = $userId;

                return $this;
        }

        /**
         * @param int $isRead
         *
         * @return self
         */
        public function setIsRead(int $isRead): self
        {
                $this->isRead = $isRead;

                return $this;
        }

        /**
         * @return string
         */
        public function getMessageId(): ?string

        {
                return $this->messageId;
        }

        /**
         * @return string
         */
        public function getUserId(): ?string

        {
                return $this->userId;
        }

        /**
         * @return int
         */
        public function getIsRead(): ?int

        {
                return $this->isRead;
        }


}
