<?php declare(strict_types=1);


namespace App\Model;

use Swoft\Db\Annotation\Mapping\Column;
use Swoft\Db\Annotation\Mapping\Entity;
use Swoft\Db\Annotation\Mapping\Id;
use Swoft\Db\Eloquent\Model;


/**
 * 系统日志表
 * Class SystemLog
 *
 * @since 2.0
 *
 * @Entity(table="system_log")
 */
class SystemLog extends Model
{
        use BaseModel;

        /**
         * 唯一id
         * @Id(incrementing=false)
         * @Column()
         *
         * @var string
         */
        private $uniqueId;

        /**
         * 来源 1总后台
         *
         * @Column()
         *
         * @var int
         */
        private $source;

        /**
         * 操作人id
         *
         * @Column()
         *
         * @var string
         */
        private $userId;

        /**
         * 操作人IP
         *
         * @Column()
         *
         * @var string
         */
        private $ip;

        /**
         * 操作路径
         *
         * @Column()
         *
         * @var string
         */
        private $path;

        /**
         * 请求方式
         *
         * @Column()
         *
         * @var string
         */
        private $method;

        /**
         * 行为
         *
         * @Column()
         *
         * @var string
         */
        private $behavior;

        /**
         * 操作前
         *
         * @Column()
         *
         * @var string
         */
        private $operationBefore;

        /**
         * 操作后
         *
         * @Column()
         *
         * @var string
         */
        private $operationAfter;

        /**
         * 条件语
         *
         * @Column()
         *
         * @var string
         */
        private $whereData;

        /**
         * 创建时间
         *
         * @Column()
         *
         * @var int
         */
        private $createTime;


        /**
         * @param string $uniqueId
         *
         * @return self
         */
        public function setUniqueId(string $uniqueId): self
        {
                $this->uniqueId = $uniqueId;

                return $this;
        }

        /**
         * @param int $source
         *
         * @return self
         */
        public function setSource(int $source): self
        {
                $this->source = $source;

                return $this;
        }

        /**
         * @param string $userId
         *
         * @return self
         */
        public function setUserId(string $userId): self
        {
                $this->userId = $userId;

                return $this;
        }

        /**
         * @param string $ip
         *
         * @return self
         */
        public function setIp(string $ip): self
        {
                $this->ip = $ip;

                return $this;
        }

        /**
         * @param string $path
         *
         * @return self
         */
        public function setPath(string $path): self
        {
                $this->path = $path;

                return $this;
        }

        /**
         * @param string $method
         *
         * @return self
         */
        public function setMethod(string $method): self
        {
                $this->method = $method;

                return $this;
        }

        /**
         * @param string $behavior
         *
         * @return self
         */
        public function setBehavior(string $behavior): self
        {
                $this->behavior = $behavior;

                return $this;
        }

        /**
         * @param string $operationBefore
         *
         * @return self
         */
        public function setOperationBefore(string $operationBefore): self
        {
                $this->operationBefore = $operationBefore;

                return $this;
        }

        /**
         * @param string $operationAfter
         *
         * @return self
         */
        public function setOperationAfter(string $operationAfter): self
        {
                $this->operationAfter = $operationAfter;

                return $this;
        }

        /**
         * @param string $whereData
         *
         * @return self
         */
        public function setWhereData(string $whereData): self
        {
                $this->whereData = $whereData;

                return $this;
        }

        /**
         * @param int $createTime
         *
         * @return self
         */
        public function setCreateTime(int $createTime): self
        {
                $this->createTime = $createTime;

                return $this;
        }

        /**
         * @return string
         */
        public function getUniqueId(): ?string

        {
                return $this->uniqueId;
        }

        /**
         * @return int
         */
        public function getSource(): ?int

        {
                return $this->source;
        }

        /**
         * @return string
         */
        public function getUserId(): ?string

        {
                return $this->userId;
        }

        /**
         * @return string
         */
        public function getIp(): ?string

        {
                return $this->ip;
        }

        /**
         * @return string
         */
        public function getPath(): ?string

        {
                return $this->path;
        }

        /**
         * @return string
         */
        public function getMethod(): ?string

        {
                return $this->method;
        }

        /**
         * @return string
         */
        public function getBehavior(): ?string

        {
                return $this->behavior;
        }

        /**
         * @return string
         */
        public function getOperationBefore(): ?string

        {
                return $this->operationBefore;
        }

        /**
         * @return string
         */
        public function getOperationAfter(): ?string

        {
                return $this->operationAfter;
        }

        /**
         * @return string
         */
        public function getWhereData(): ?string

        {
                return $this->whereData;
        }

        /**
         * @return int
         */
        public function getCreateTime(): ?int

        {
                return $this->createTime;
        }


}
