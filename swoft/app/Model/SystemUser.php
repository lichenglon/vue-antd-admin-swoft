<?php declare(strict_types=1);


namespace App\Model;

use Swoft\Db\Annotation\Mapping\Column;
use Swoft\Db\Annotation\Mapping\Entity;
use Swoft\Db\Annotation\Mapping\Id;
use Swoft\Db\Eloquent\Model;


/**
 * 系统用户表
 * Class SystemUser
 *
 * @since 2.0
 *
 * @Entity(table="system_user")
 */
class SystemUser extends Model
{
        use BaseModel;

        /**
         * 用户id
         * @Id(incrementing=false)
         * @Column()
         *
         * @var string
         */
        private $userId;

        /**
         * 头像
         *
         * @Column()
         *
         * @var string
         */
        private $avatar;

        /**
         * 真实姓名
         *
         * @Column()
         *
         * @var string
         */
        private $realName;

        /**
         * 手机号
         *
         * @Column()
         *
         * @var string
         */
        private $phone;

        /**
         * 用户名
         *
         * @Column()
         *
         * @var string
         */
        private $username;

        /**
         * 密码
         *
         * @Column(hidden=true)
         *
         * @var string
         */
        private $password;

        /**
         * 随机加密盐
         *
         * @Column()
         *
         * @var string
         */
        private $salt;

        /**
         * 状态 1启用 2禁用
         *
         * @Column()
         *
         * @var int
         */
        private $status;

        /**
         * 创建时间
         *
         * @Column()
         *
         * @var int
         */
        private $createTime;

        /**
         * 更新时间
         *
         * @Column()
         *
         * @var int
         */
        private $updateTime;

        /**
         * 最近登录时间
         *
         * @Column()
         *
         * @var int
         */
        private $lastTime;

        /**
         * 最近登录IP
         *
         * @Column()
         *
         * @var string
         */
        private $lastIp;


        /**
         * @param string $userId
         *
         * @return self
         */
        public function setUserId(string $userId): self
        {
                $this->userId = $userId;

                return $this;
        }

        /**
         * @param string $avatar
         *
         * @return self
         */
        public function setAvatar(string $avatar): self
        {
                $this->avatar = $avatar;

                return $this;
        }

        /**
         * @param string $realName
         *
         * @return self
         */
        public function setRealName(string $realName): self
        {
                $this->realName = $realName;

                return $this;
        }

        /**
         * @param string $phone
         *
         * @return self
         */
        public function setPhone(string $phone): self
        {
                $this->phone = $phone;

                return $this;
        }

        /**
         * @param string $username
         *
         * @return self
         */
        public function setUsername(string $username): self
        {
                $this->username = $username;

                return $this;
        }

        /**
         * @param string $password
         *
         * @return self
         */
        public function setPassword(string $password): self
        {
                $this->password = $password;

                return $this;
        }

        /**
         * @param string $salt
         *
         * @return self
         */
        public function setSalt(string $salt): self
        {
                $this->salt = $salt;

                return $this;
        }

        /**
         * @param int $status
         *
         * @return self
         */
        public function setStatus(int $status): self
        {
                $this->status = $status;

                return $this;
        }

        /**
         * @param int $createTime
         *
         * @return self
         */
        public function setCreateTime(int $createTime): self
        {
                $this->createTime = $createTime;

                return $this;
        }

        /**
         * @param int $updateTime
         *
         * @return self
         */
        public function setUpdateTime(int $updateTime): self
        {
                $this->updateTime = $updateTime;

                return $this;
        }

        /**
         * @param int $lastTime
         *
         * @return self
         */
        public function setLastTime(int $lastTime): self
        {
                $this->lastTime = $lastTime;

                return $this;
        }

        /**
         * @param string $lastIp
         *
         * @return self
         */
        public function setLastIp(string $lastIp): self
        {
                $this->lastIp = $lastIp;

                return $this;
        }

        /**
         * @return string
         */
        public function getUserId(): ?string

        {
                return $this->userId;
        }

        /**
         * @return string
         */
        public function getAvatar(): ?string

        {
                return $this->avatar;
        }

        /**
         * @return string
         */
        public function getRealName(): ?string

        {
                return $this->realName;
        }

        /**
         * @return string
         */
        public function getPhone(): ?string

        {
                return $this->phone;
        }

        /**
         * @return string
         */
        public function getUsername(): ?string

        {
                return $this->username;
        }

        /**
         * @return string
         */
        public function getPassword(): ?string

        {
                return $this->password;
        }

        /**
         * @return string
         */
        public function getSalt(): ?string

        {
                return $this->salt;
        }

        /**
         * @return int
         */
        public function getStatus(): ?int

        {
                return $this->status;
        }

        /**
         * @return int
         */
        public function getCreateTime(): ?int

        {
                return $this->createTime;
        }

        /**
         * @return int
         */
        public function getUpdateTime(): ?int

        {
                return $this->updateTime;
        }

        /**
         * @return int
         */
        public function getLastTime(): ?int

        {
                return $this->lastTime;
        }

        /**
         * @return string
         */
        public function getLastIp(): ?string

        {
                return $this->lastIp;
        }


}
