<?php declare(strict_types=1);


namespace App\Model;

use Swoft\Db\Annotation\Mapping\Column;
use Swoft\Db\Annotation\Mapping\Entity;
use Swoft\Db\Annotation\Mapping\Id;
use Swoft\Db\Eloquent\Model;


/**
 * 系统角色表
 * Class SystemRole
 *
 * @since 2.0
 *
 * @Entity(table="system_role")
 */
class SystemRole extends Model
{
        use BaseModel;

        /**
         * 角色id
         * @Id(incrementing=false)
         * @Column()
         *
         * @var string
         */
        private $roleId;

        /**
         * 角色名称
         *
         * @Column()
         *
         * @var string
         */
        private $name;

        /**
         * 排序
         *
         * @Column()
         *
         * @var int
         */
        private $sort;

        /**
         * 状态 1启用 2禁用
         *
         * @Column()
         *
         * @var int
         */
        private $status;

        /**
         * 创建时间
         *
         * @Column()
         *
         * @var int
         */
        private $createTime;

        /**
         * 更新时间
         *
         * @Column()
         *
         * @var int
         */
        private $updateTime;


        /**
         * @param string $roleId
         *
         * @return self
         */
        public function setRoleId(string $roleId): self
        {
                $this->roleId = $roleId;

                return $this;
        }

        /**
         * @param string $name
         *
         * @return self
         */
        public function setName(string $name): self
        {
                $this->name = $name;

                return $this;
        }

        /**
         * @param int $sort
         *
         * @return self
         */
        public function setSort(int $sort): self
        {
                $this->sort = $sort;

                return $this;
        }

        /**
         * @param int $status
         *
         * @return self
         */
        public function setStatus(int $status): self
        {
                $this->status = $status;

                return $this;
        }

        /**
         * @param int $createTime
         *
         * @return self
         */
        public function setCreateTime(int $createTime): self
        {
                $this->createTime = $createTime;

                return $this;
        }

        /**
         * @param int $updateTime
         *
         * @return self
         */
        public function setUpdateTime(int $updateTime): self
        {
                $this->updateTime = $updateTime;

                return $this;
        }

        /**
         * @return string
         */
        public function getRoleId(): ?string

        {
                return $this->roleId;
        }

        /**
         * @return string
         */
        public function getName(): ?string

        {
                return $this->name;
        }

        /**
         * @return int
         */
        public function getSort(): ?int

        {
                return $this->sort;
        }

        /**
         * @return int
         */
        public function getStatus(): ?int

        {
                return $this->status;
        }

        /**
         * @return int
         */
        public function getCreateTime(): ?int

        {
                return $this->createTime;
        }

        /**
         * @return int
         */
        public function getUpdateTime(): ?int

        {
                return $this->updateTime;
        }


}
