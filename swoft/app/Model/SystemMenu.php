<?php declare(strict_types=1);


namespace App\Model;

use Swoft\Db\Annotation\Mapping\Column;
use Swoft\Db\Annotation\Mapping\Entity;
use Swoft\Db\Annotation\Mapping\Id;
use Swoft\Db\Eloquent\Model;


/**
 * 系统菜单表
 * Class SystemMenu
 *
 * @since 2.0
 *
 * @Entity(table="system_menu")
 */
class SystemMenu extends Model
{
        use BaseModel;

        /**
         * 菜单id
         * @Id(incrementing=false)
         * @Column()
         *
         * @var string
         */
        private $menuId;

        /**
         * 父id
         *
         * @Column()
         *
         * @var string
         */
        private $pid;

        /**
         * 关联的菜单id
         *
         * @Column()
         *
         * @var string
         */
        private $reMenuId;

        /**
         * 菜单名称
         *
         * @Column()
         *
         * @var string
         */
        private $name;

        /**
         * 图标
         *
         * @Column()
         *
         * @var string
         */
        private $icon;

        /**
         * 路由
         *
         * @Column()
         *
         * @var string
         */
        private $route;

        /**
         * 路径
         *
         * @Column()
         *
         * @var string
         */
        private $path;

        /**
         * json url形式参数
         *
         * @Column()
         *
         * @var string
         */
        private $urlParams;

        /**
         * json path形式参数
         *
         * @Column()
         *
         * @var string
         */
        private $pathParams;

        /**
         * 类型 1菜单 2按钮
         *
         * @Column()
         *
         * @var int
         */
        private $menuType;

        /**
         * 打开方式 1内部打开 2外部打开
         *
         * @Column()
         *
         * @var int
         */
        private $openType;

        /**
         * 状态 1启用 2禁用
         *
         * @Column()
         *
         * @var int
         */
        private $status;

        /**
         * 请求方式
         *
         * @Column()
         *
         * @var string
         */
        private $method;

        /**
         * 权限辨别
         *
         * @Column()
         *
         * @var string
         */
        private $powerDiscern;

        /**
         * 排序
         *
         * @Column()
         *
         * @var int
         */
        private $sort;


        /**
         * @param string $menuId
         *
         * @return self
         */
        public function setMenuId(string $menuId): self
        {
                $this->menuId = $menuId;

                return $this;
        }

        /**
         * @param string $pid
         *
         * @return self
         */
        public function setPid(string $pid): self
        {
                $this->pid = $pid;

                return $this;
        }

        /**
         * @param string $reMenuId
         *
         * @return self
         */
        public function setReMenuId(string $reMenuId): self
        {
                $this->reMenuId = $reMenuId;

                return $this;
        }

        /**
         * @param string $name
         *
         * @return self
         */
        public function setName(string $name): self
        {
                $this->name = $name;

                return $this;
        }

        /**
         * @param string $icon
         *
         * @return self
         */
        public function setIcon(string $icon): self
        {
                $this->icon = $icon;

                return $this;
        }

        /**
         * @param string $route
         *
         * @return self
         */
        public function setRoute(string $route): self
        {
                $this->route = $route;

                return $this;
        }

        /**
         * @param string $path
         *
         * @return self
         */
        public function setPath(string $path): self
        {
                $this->path = $path;

                return $this;
        }

        /**
         * @param string $urlParams
         *
         * @return self
         */
        public function setUrlParams(string $urlParams): self
        {
                $this->urlParams = $urlParams;

                return $this;
        }

        /**
         * @param string $pathParams
         *
         * @return self
         */
        public function setPathParams(string $pathParams): self
        {
                $this->pathParams = $pathParams;

                return $this;
        }

        /**
         * @param int $menuType
         *
         * @return self
         */
        public function setMenuType(int $menuType): self
        {
                $this->menuType = $menuType;

                return $this;
        }

        /**
         * @param int $openType
         *
         * @return self
         */
        public function setOpenType(int $openType): self
        {
                $this->openType = $openType;

                return $this;
        }

        /**
         * @param int $status
         *
         * @return self
         */
        public function setStatus(int $status): self
        {
                $this->status = $status;

                return $this;
        }

        /**
         * @param string $method
         *
         * @return self
         */
        public function setMethod(string $method): self
        {
                $this->method = $method;

                return $this;
        }

        /**
         * @param string $powerDiscern
         *
         * @return self
         */
        public function setPowerDiscern(string $powerDiscern): self
        {
                $this->powerDiscern = $powerDiscern;

                return $this;
        }

        /**
         * @param int $sort
         *
         * @return self
         */
        public function setSort(int $sort): self
        {
                $this->sort = $sort;

                return $this;
        }

        /**
         * @return string
         */
        public function getMenuId(): ?string

        {
                return $this->menuId;
        }

        /**
         * @return string
         */
        public function getPid(): ?string

        {
                return $this->pid;
        }

        /**
         * @return string
         */
        public function getReMenuId(): ?string

        {
                return $this->reMenuId;
        }

        /**
         * @return string
         */
        public function getName(): ?string

        {
                return $this->name;
        }

        /**
         * @return string
         */
        public function getIcon(): ?string

        {
                return $this->icon;
        }

        /**
         * @return string
         */
        public function getRoute(): ?string

        {
                return $this->route;
        }

        /**
         * @return string
         */
        public function getPath(): ?string

        {
                return $this->path;
        }

        /**
         * @return string
         */
        public function getUrlParams(): ?string

        {
                return $this->urlParams;
        }

        /**
         * @return string
         */
        public function getPathParams(): ?string

        {
                return $this->pathParams;
        }

        /**
         * @return int
         */
        public function getMenuType(): ?int

        {
                return $this->menuType;
        }

        /**
         * @return int
         */
        public function getOpenType(): ?int

        {
                return $this->openType;
        }

        /**
         * @return int
         */
        public function getStatus(): ?int

        {
                return $this->status;
        }

        /**
         * @return string
         */
        public function getMethod(): ?string

        {
                return $this->method;
        }

        /**
         * @return string
         */
        public function getPowerDiscern(): ?string

        {
                return $this->powerDiscern;
        }

        /**
         * @return int
         */
        public function getSort(): ?int

        {
                return $this->sort;
        }


}
