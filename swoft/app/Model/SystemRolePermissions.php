<?php declare(strict_types=1);


namespace App\Model;

use Swoft\Db\Annotation\Mapping\Column;
use Swoft\Db\Annotation\Mapping\Entity;
use Swoft\Db\Annotation\Mapping\Id;
use Swoft\Db\Eloquent\Model;


/**
 * 系统角色权限表
 * Class SystemRolePermissions
 *
 * @since 2.0
 *
 * @Entity(table="system_role_permissions")
 */
class SystemRolePermissions extends Model
{
        use BaseModel;

        /**
         * 角色id
         *
         * @Column()
         *
         * @var string
         */
        private $roleId;

        /**
         * 菜单id
         *
         * @Column()
         *
         * @var string
         */
        private $menuId;

        /**
         * 是否伪选中 1是 2否 树控件当选中子节点父节点是伪选中状态的
         *
         * @Column()
         *
         * @var int
         */
        private $pseudoChecked;


        /**
         * @param string $roleId
         *
         * @return self
         */
        public function setRoleId(string $roleId): self
        {
                $this->roleId = $roleId;

                return $this;
        }

        /**
         * @param string $menuId
         *
         * @return self
         */
        public function setMenuId(string $menuId): self
        {
                $this->menuId = $menuId;

                return $this;
        }

        /**
         * @param int $pseudoChecked
         *
         * @return self
         */
        public function setPseudoChecked(int $pseudoChecked): self
        {
                $this->pseudoChecked = $pseudoChecked;

                return $this;
        }

        /**
         * @return string
         */
        public function getRoleId(): ?string

        {
                return $this->roleId;
        }

        /**
         * @return string
         */
        public function getMenuId(): ?string

        {
                return $this->menuId;
        }

        /**
         * @return int
         */
        public function getPseudoChecked(): ?int

        {
                return $this->pseudoChecked;
        }


}
