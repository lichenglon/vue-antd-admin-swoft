<?php declare(strict_types=1);

namespace App\Http\Business\Admin;

use App\Model\SystemUploadLog;
use Intervention\Image\ImageManager;

/**
 * 系统上传文件记录-业务类
 * Class SystemUploadLogBusiness
 * @package App\Http\Business\Admin
 */
class SystemUploadLogBusiness
{
        /**
         * 上传图片
         * @param int $source
         * @return array
         */
        public static function uploadPic(int $source): array
        {
                $objFile = request()->getUploadedFiles()['file'];
                if ($objFile->getError() !== UPLOAD_ERR_OK) return callbackParam(304, false, [], '上传失败,文件错误或损坏');
                $floatSize = $objFile->getSize();
                $strClientFilename = $objFile->getClientFilename();
                $strClientMediaType = $objFile->getClientMediaType();
                $strSuffix = strtolower(substr(strrchr($strClientFilename, '.'), 1));
                $floatUploadPicMax = 10;
                if ($floatSize > ($floatUploadPicMax * 1048576)) {
                        return ['code' => 304, 'msg' => '上传失败，文件超出限制大小，允许上传大小' . $floatUploadPicMax . 'MB'];
                }
                $strUploadPicSuffix = 'jpg,png,gif,jpeg';
                if (!in_array($strSuffix, explode(',', $strUploadPicSuffix))) {
                        return callbackParam(304, false, [], '上传失败，文件格式不合法，允许上传格式' . $strUploadPicSuffix);
                }
                $strDate = date('Ymd', request()->timestamp);
                $strUploadDir = 'public/uploads/pic/' . $strDate . '/';
                if (!is_dir(BASE_PATH . $strUploadDir)) @mkdir(BASE_PATH . $strUploadDir, 0755);
                $strId = uuid();
                $strFilename = sha1($strId);
                $strSaveLocation = $strUploadDir . $strFilename . '.' . $strSuffix;
                $objFile->moveTo(BASE_PATH . $strSaveLocation);
                //$strThumbnailSaveLocation = $strUploadDir . $strFilename . '_thumbnail.' . $strSuffix;
                //self::makeThumbnail(BASE_PATH . $strSaveLocation, BASE_PATH . $strThumbnailSaveLocation);
                $strFilePath = '/' . $strSaveLocation;
                //$strThumbFilePath = '/' . $strThumbnailSaveLocation;
                $arrSet = [
                        'fileLogId' => $strId,
                        'userId' => request()->userId,
                        'source' => $source,
                        'type' => 1,
                        'size' => round($floatSize / 1048576, 2),
                        'clientFilename' => $strClientFilename,
                        'mediaType' => $strClientMediaType,
                        'filePath' => $strFilePath,
                        'createTime' => request()->timestamp
                ];
                SystemUploadLog::insert($arrSet);
                $strDomain = getDomain();
                $arrResult = [
                        'fileLogId' => $strId,
                        'filePath' => $strDomain . $strFilePath,
                        //'thumbFilePath' => $strDomain . $strThumbFilePath,
                ];
                return callbackParam(0, true, $arrResult, '上传成功');
        }

        /**
         * 生成缩略图
         * @param string $strSourcePath
         * @param string $strSaveLocation
         */
        protected static function makeThumbnail(string $strSourcePath, string $strSaveLocation): void
        {
                $manager = new ImageManager(array('driver' => 'imagick'));
                $intResizeWidth = ceil(getimagesize($strSourcePath)[0] / 2);
                $thumb = $manager->make($strSourcePath)->orientate()->resize((int) $intResizeWidth, null, function ($constraint) {
                        $constraint->aspectRatio();
                });
                $thumb->save($strSaveLocation);
        }
}