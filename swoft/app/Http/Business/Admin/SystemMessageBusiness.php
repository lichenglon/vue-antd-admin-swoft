<?php declare(strict_types=1);

namespace App\Http\Business\Admin;

use App\Model\SystemMessage;
use App\Model\SystemMessageUser;

/**
 * 消息通知待办-业务类
 * Class SystemMessageBusiness
 * @package App\Http\Business\Admin
 */
class SystemMessageBusiness
{
        /**
         * 查询
         * @param string $messageId
         * @return array
         */
        public static function query(string $messageId): array
        {
                if ($messageId) {
                        $arrField = ['messageId', 'title', 'content', 'type', 'userId', 'createTime'];
                        $arrResult = SystemMessage::primaryKeyFind($messageId, $arrField);
                        $arrWhere = ['messageId' => $messageId];
                        $strColumn = 'userId';
                        $arrResult['messageUserId'] = SystemMessageUser::getColumn($arrWhere, $strColumn);
                } else {
                        $arrParam = get();
                        $intPage = intval(issetArrKey($arrParam, 'page', 1));
                        $intLimit = intval(issetArrKey($arrParam, 'limit', 10));
                        $arrWhere = [];
                        if (isset($arrParam['title']) && $arrParam['title']) $arrWhere[] = ['title', 'like', "%{$arrParam['title']}%"];
                        if (isset($arrParam['type'])) $arrWhere['type'] = intval($arrParam['type']);
                        if (isset($arrParam['createTime']) && $arrParam['createTime']) $arrWhere[] = ['whereBetween', 'createTime', array_map('strtotime', $arrParam['createTime'])];
                        if (isset($arrParam['updateTime']) && $arrParam['updateTime']) $arrWhere[] = ['whereBetween', 'updateTime', array_map('strtotime', $arrParam['updateTime'])];
                        $arrField = ['messageId', 'title', 'content', 'type', 'userId', 'createTime'];
                        $arrOrderBy = ['createTime' => 'DESC'];
                        $arrResult = SystemMessage::getList($arrWhere, $arrField, $intPage, $intLimit, $arrOrderBy);
                        $arrMessageId = array_column($arrResult['list'], 'messageId');
                        $arrWhere = [['whereIn', 'messageId', $arrMessageId]];
                        $arrField = ['messageId', 'userId'];
                        $arrMessageUserId = SystemMessageUser::getAll($arrWhere, $arrField);
                        $arrMessageUserId = arrayGroupBy($arrMessageUserId, 'messageId');
                        foreach ($arrResult['list'] as $k => $v) {
                                $arrResult['list'][$k]['messageUserId'] = array_column($arrMessageUserId[$v['messageId']], 'userId');
                        }
                        $arrResult['page'] = $intPage;
                        $arrResult['limit'] = $intLimit;
                }
                return callbackParam(0, true, $arrResult);
        }

        /**
         * 新增
         * @return array
         * @throws \Exception
         */
        public static function add(): array
        {
                $arrParam = post();
                $strMessageId = uuid();
                $arrSet = [
                        'messageId' => $strMessageId,
                        'title' => strAddslashes($arrParam['title']),
                        'content' => strAddslashes($arrParam['content']),
                        'type' => intval($arrParam['type']),
                        'userId' => request()->userId,
                        'createTime' => request()->timestamp
                ];
                $bool = SystemMessage::insert($arrSet);
                if (!$bool) throw new \Exception('服务器处理数据失败');
                $arrMessageUserSet = [];
                foreach ($arrParam['userIds'] as $v) {
                        $arrMessageUserSet[] = [
                                'messageId' => $strMessageId,
                                'userId' => strAddslashes($v)
                        ];
                }
                SystemMessageUser::insert($arrMessageUserSet);
                $arrSet['userIds'] = $arrParam['userIds'];
                $arrSystemLogData = ['behavior' => '新增消息通知', 'operationAfter' => $arrSet];
                setSystemLog($arrSystemLogData);
                $arrResult = ['messageId' => $strMessageId];
                return callbackParam(0, true, $arrResult, '新增成功');
        }

        /**
         * 删除
         * @param string $messageId
         * @return array
         */
        public static function delete(string $messageId): array
        {
                $arrOperationBefore = self::query($messageId)['data'];
                if (!$arrOperationBefore) throw new \Exception('数据不存在');
                $arrWhere = ['messageId' => strAddslashes($messageId)];
                $bool = SystemMessage::remove($arrWhere);
                if (!$bool) throw new \Exception('服务器处理数据失败');
                SystemMessageUser::remove($arrWhere);
                $arrSystemLogData = ['behavior' => '删除消息通知', 'operationBefore' => $arrOperationBefore, 'whereData' => $arrWhere];
                setSystemLog($arrSystemLogData);
                return callbackParam(0, true, [], '删除成功');
        }
}