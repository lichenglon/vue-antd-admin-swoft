<?php declare(strict_types=1);

namespace App\Http\Business\Admin;

use App\Model\SystemMenu;

/**
 * 菜单-业务类
 * Class SystemMenuBusiness
 * @package App\Http\Business\Admin
 */
class SystemMenuBusiness
{
        /**
         * 查询菜单
         * @return array
         */
        public static function query(string $menuId): array
        {
                $arrField = ['menuId', 'pid', 'reMenuId', 'name', 'icon', 'route', 'path', 'urlParams', 'pathParams', 'menuType', 'openType', 'status', 'method', 'powerDiscern', 'sort'];
                if ($menuId) {
                        $arrResult = SystemMenu::primaryKeyFind(strAddslashes($menuId), $arrField);
                        if (isset($arrResult['reMenuId'])) $arrResult['reMenuId'] = $arrResult['reMenuId'] ? explode(',', $arrResult['reMenuId']) : [];
                } else {
                        $arrWhere = [];
                        $arrOrderBy = ['sort' => 'DESC'];
                        $arrResult = SystemMenu::getAll($arrWhere, $arrField, $arrOrderBy);
                }
                return callbackParam(0, true, $arrResult);
        }

        /**
         * 新增菜单
         * @return array
         */
        public static function add(): array
        {
                $arrParam = post();
                $strMenuId = uuid();
                $intMenuType = intval($arrParam['menuType']);
                $arrSet = [
                        'menuId' => $strMenuId,
                        'pid' => strAddslashes(issetArrKey($arrParam, 'pid', '')),
                        'reMenuId' => isset($arrParam['reMenuId']) ? implode(',', strAddslashes($arrParam['reMenuId'])) : '',
                        'name' => strAddslashes($arrParam['name']),
                        'icon' => strAddslashes(issetArrKey($arrParam, 'icon', '')),
                        'route' => strAddslashes(issetArrKey($arrParam, 'route', '')),
                        'path' => strAddslashes(issetArrKey($arrParam, 'path', '')),
                        'urlParams' => isset($arrParam['urlParams']) ? json_encode($arrParam['urlParams'], JSON_UNESCAPED_UNICODE) : '',
                        'pathParams' => isset($arrParam['pathParams']) ? json_encode($arrParam['pathParams'], JSON_UNESCAPED_UNICODE) : '',
                        'menuType' => $intMenuType,
                        'openType' => intval($arrParam['openType']),
                        'status' => intval($arrParam['status']),
                        'method' => strAddslashes($arrParam['method']),
                        'powerDiscern' => strAddslashes($arrParam['powerDiscern']),
                        'sort' => intval(issetArrKey($arrParam, 'sort', 1))
                ];
                SystemMenu::insert($arrSet);
                $arrSystemLogData = ['behavior' => '新增菜单', 'operationAfter' => $arrSet];
                setSystemLog($arrSystemLogData);
                $arrResult = ['menuId' => $strMenuId];
                return callbackParam(0, true, $arrResult, '新增成功');
        }

        /**
         * 更新菜单
         * @param string $menuId
         * @return array
         */
        public static function edit(string $menuId): array
        {
                $arrParam = post();
                $arrOperationBefore = self::query($menuId)['data'];
                if (!$arrOperationBefore) throw new \Exception('数据不存在');
                $arrWhere = ['menuId' => strAddslashes($menuId)];
                $arrSet = [
                        'pid' => strAddslashes(issetArrKey($arrParam, 'pid', '')),
                        'reMenuId' => isset($arrParam['reMenuId']) ? implode(',', strAddslashes($arrParam['reMenuId'])) : '',
                        'name' => strAddslashes($arrParam['name']),
                        'icon' => strAddslashes(issetArrKey($arrParam, 'icon', '')),
                        'route' => strAddslashes(issetArrKey($arrParam, 'route', '')),
                        'path' => strAddslashes(issetArrKey($arrParam, 'path', '')),
                        'urlParams' => isset($arrParam['urlParams']) ? json_encode($arrParam['urlParams'], JSON_UNESCAPED_UNICODE) : '',
                        'pathParams' => isset($arrParam['pathParams']) ? json_encode($arrParam['pathParams'], JSON_UNESCAPED_UNICODE) : '',
                        'menuType' => intval($arrParam['menuType']),
                        'openType' => intval($arrParam['openType']),
                        'status' => intval($arrParam['status']),
                        'method' => strAddslashes($arrParam['method']),
                        'powerDiscern' => strAddslashes($arrParam['powerDiscern']),
                        'sort' => intval(issetArrKey($arrParam, 'sort', 1))
                ];
                SystemMenu::renewal($arrWhere, $arrSet);
                $arrSystemLogData = ['behavior' => '编辑菜单', 'operationBefore' => $arrOperationBefore, 'operationAfter' => $arrSet, 'whereData' => $arrWhere];
                setSystemLog($arrSystemLogData);
                return callbackParam(0, true, [], '编辑成功');
        }

        /**
         * 编辑指定指定
         * @param string $menuId
         * @param string $field
         * @return array
         * @throws \Exception
         */
        public static function editSpecifiedField(string $menuId, string $field): array
        {
                $arrOperationBefore = SystemMenu::primaryKeyFind($menuId, [strAddslashes($field)]);
                if (!$arrOperationBefore) throw new \Exception('数据不存在');
                switch ($field) {
                        case 'status':
                                $arrWhere = ['pid' => strAddslashes($menuId)];
                                $strCheck = SystemMenu::getFieldValue($arrWhere, 'menuId');
                                if ($strCheck) throw new \Exception('请先禁用子菜单');
                                $arrSet = ['status' => intval(post('status'))];
                                break;
                        default :
                                throw new \Exception('未知字段');
                }
                $arrWhere = ['menuId' => strAddslashes($menuId)];
                SystemMenu::renewal($arrWhere, $arrSet);
                $arrSystemLogData = ['behavior' => '编辑菜单指定字段', 'operationBefore' => $arrOperationBefore, 'operationAfter' => $arrSet, 'whereData' => $arrWhere];
                setSystemLog($arrSystemLogData);
                return callbackParam(0, true, [], '编辑成功');
        }

        /**
         * 删除菜单
         * @param string $menuId
         * @return array
         * @throws \Exception
         */
        public static function delete(string $menuId): array
        {
                $arrOperationBefore = self::query($menuId)['data'];
                if (!$arrOperationBefore) throw new \Exception('数据不存在');
                $arrWhere = ['pid' => strAddslashes($menuId)];
                $strCheck = SystemMenu::getFieldValue($arrWhere, 'menuId');
                if ($strCheck) throw new \Exception('请先删除子菜单');
                $arrWhere = ['menuId' => strAddslashes($menuId)];
                SystemMenu::remove($arrWhere);
                $arrSystemLogData = ['behavior' => '删除菜单', 'operationBefore' => $arrOperationBefore, 'whereData' => $arrWhere];
                setSystemLog($arrSystemLogData);
                return callbackParam(0, true, [], '删除成功');
        }
}