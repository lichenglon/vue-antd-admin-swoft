<?php declare(strict_types=1);

namespace App\Business;

use App\Model\Kid\KidPhysicalTestResult as KidPhysicalTestResultModel;
use App\Model\Kid\KidPhysicalTestResultLog as KidPhysicalTestResultLogModel;
use App\Business\Project\Project as ProjectBusiness;
use App\Business\Project\ProjectContent as ProjectContentBusiness;
use App\Business\PhysicalTest\PhysicalTest as PhysicalTestBusiness;
use App\Model\PhysicalTest\PhysicalTestClasses as PhysicalTestClassesModel;
use App\Business\Kid\Kid as KidBusiness;
use App\Model\PhysicalTest\PhysicalTestProject as PhysicalTestProjectModel;
use App\Model\Project\ProjectStandard as ProjectStandardModel;
use Swoft\Db\DB;
use App\Model\Project\Project as ProjectModel;
use App\Model\Kid\Kid as KidModel;
use App\Model\PhysicalTest\PhysicalTest as PhysicalTestModel;
use App\Business\PhysicalTest\PhysicalTestScoreName as PhysicalTestScoreNameBusiness;
use App\Model\PhysicalTest\PhysicalTestScoreName as PhysicalTestScoreNameModel;


class Test
{
        /**
         * 获取健康预警
         * @param array $arrParameter
         * @return array
         * @throws \Exception
         */
        public static function getWarning(array $arrParameter): array
        {
                $strInstitutionId = strAddslashes(trim($arrParameter['institutionId']));
                $strClassesId = strAddslashes(trim($arrParameter['classesId']));
                $arrWhere = [
                        'physical_test.status' => 2,
                        'physical_test.institutionId' => $strInstitutionId,
                        'physical_test_classes.classesId' => $strClassesId
                ];
                $arrJoin = [
                        'physical_test_classes' => [
                                'table' => 'physical_test_classes',
                                'where' => ['physical_test_classes.physicalTestId', '=', 'physical_test.physicalTestId']
                        ]
                ];
                $arrField = ['physical_test.physicalTestId'];
                $arrOrderBy = ['physical_test.startTime' => 'DESC'];
                $arrGroupBy = [];
                $arrPhysicalTest = PhysicalTestModel::getOne($arrWhere, $arrField, $arrOrderBy, $arrGroupBy, $arrJoin);
                if (!$arrPhysicalTest) {
                        return [
                                'code' => 0,
                                'msg' => '获取成功',
                                'data' => []
                        ];
                }
                $strPhysicalTestId = $arrPhysicalTest['physicalTestId'];
                $strWeightProjectId = 'lclce204f56-3940-d0ff-1d3a-a0f94f4c1e6f';
                $strHeightProjectId = 'lcl6e58f486-d555-176a-805e-376ac3935dfd';
                $arrWhere = [
                        'isTest' => 1,
                        'institutionId' => $strInstitutionId,
                        'physicalTestId' => $strPhysicalTestId,
                        'classesId' => $strClassesId,
                        'projectId' => $strWeightProjectId
                ];
                $intWeightTotalNumber = KidPhysicalTestResultModel::getTotal($arrWhere);
                $arrWhere['score'] = 1;
                $intWeightWarningNumber = KidPhysicalTestResultModel::getTotal($arrWhere);
                $floatWeightProportion = ($intWeightTotalNumber > 0) ? (($intWeightWarningNumber / $intWeightTotalNumber) * 100) : 0;
                unset($arrWhere['score']);
                $arrWhere['projectId'] = $strHeightProjectId;
                $intHeightTotalNumber = KidPhysicalTestResultModel::getTotal($arrWhere);
                $arrWhere['score'] = 1;
                $intHeightWarningNumber = KidPhysicalTestResultModel::getTotal($arrWhere);
                $floatHeightProportion = ($intHeightTotalNumber > 0) ? (($intHeightWarningNumber / $intHeightTotalNumber) * 100) : 0;
                $arrData = [
                        [
                                'name' => '体重不达标',
                                'totalNumber' => $intWeightTotalNumber,
                                'warningNumber' => $intWeightWarningNumber,
                                'proportion' => floatval(round($floatWeightProportion, 2))
                        ],
                        [
                                'name' => '身高偏低',
                                'totalNumber' => $intHeightTotalNumber,
                                'warningNumber' => $intHeightWarningNumber,
                                'proportion' => floatval(round($floatHeightProportion, 2))
                        ]
                ];
                return [
                        'code' => 0,
                        'msg' => '获取成功',
                        'data' => $arrData
                ];
        }
}