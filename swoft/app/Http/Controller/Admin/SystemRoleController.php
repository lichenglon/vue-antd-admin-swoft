<?php declare(strict_types=1);

namespace App\Http\Controller\Admin;

use Swoft\Http\Server\Annotation\Mapping\Controller;
use Swoft\Http\Server\Annotation\Mapping\RequestMapping;
use Swoft\Http\Server\Annotation\Mapping\RequestMethod;
use Swoft\Http\Message\Response;
use App\Http\Business\Admin\SystemRoleBusiness;
use Swoft\Validator\Annotation\Mapping\Validate;
use Swoft\Validator\Annotation\Mapping\ValidateType;
use Swoft\Http\Server\Annotation\Mapping\Middleware;
use App\Http\Middleware\AdminLoginMiddleware;
use App\Http\Middleware\AdminAucMiddleware;

/**
 * 系统角色-控制器
 * Class SystemRoleController
 * @package App\Http\Controller\Admin
 * @Controller(prefix="/admin")
 * @Middleware(AdminLoginMiddleware::class)
 * @Middleware(AdminAucMiddleware::class)
 */
class SystemRoleController
{
        /**
         * 查询角色
         * @RequestMapping(route="role[/{roleId}]", method={RequestMethod::GET})
         * @param string $roleId
         * @return Response
         */
        public function query(string $roleId): Response
        {
                return jsonResponse(SystemRoleBusiness::query($roleId));
        }

        /**
         * 新增角色
         * @RequestMapping(route="role", method={RequestMethod::POST})
         * @Validate(validator="SystemRoleValidator", fields={"name", "sort", "status"}, type=ValidateType::BODY)
         * @return Response
         * @throws \Exception
         */
        public function add(): Response
        {
                return jsonResponse(SystemRoleBusiness::add());
        }

        /**
         * 编辑角色
         * @RequestMapping(route="role/{roleId}[/{field}]", method={RequestMethod::PUT})
         * @Validate(validator="SystemRoleValidator", fields={"roleId"}, type=ValidateType::PATH)
         * @param string $roleId
         * @param string $field 指定编辑字段
         * @return Response
         * @throws \Exception
         */
        public function edit(string $roleId, string $field): Response
        {
                if ($field) {
                        validate(post(), 'SystemRoleValidator', [$field]);
                        return jsonResponse(SystemRoleBusiness::editSpecifiedField($roleId, $field));
                }
                validate(post(), 'SystemRoleValidator', ['name', 'sort', 'status']);
                return jsonResponse(SystemRoleBusiness::edit($roleId));
        }

        /**
         * 删除角色
         * @RequestMapping(route="role/{roleId}", method={RequestMethod::DELETE})
         * @Validate(validator="SystemRoleValidator", fields={"roleId"}, type=ValidateType::PATH)
         * @param string $roleId
         * @return Response
         * @throws \Exception
         */
        public function delete(string $roleId): Response
        {
                return jsonResponse(SystemRoleBusiness::delete($roleId));
        }

        /**
         * 分配权限
         * 揭示透露角色权限
         * @RequestMapping(route="role/allotPermissions/{roleId}", method={RequestMethod::GET})
         * @Validate(validator="SystemRoleValidator", fields={"roleId"}, type=ValidateType::PATH)
         * @param string $roleId
         * @return Response
         */
        public function revealRolePermissions(string $roleId): Response
        {
                return jsonResponse(SystemRoleBusiness::revealRolePermissions($roleId));
        }

        /**
         * 分配权限
         * 设置角色权限
         * @RequestMapping(route="role/allotPermissions/{roleId}", method={RequestMethod::POST})
         * @Validate(validator="SystemRoleValidator", fields={"roleId"}, type=ValidateType::PATH)
         * @Validate(validator="SystemRoleValidator", fields={"permissionsData"}, type=ValidateType::BODY)
         * @param string $roleId
         * @return Response
         */
        public function setRolePermissions(string $roleId): Response
        {
                foreach (post('permissionsData') as $v) {
                        validate($v, 'SystemMenuValidator', ['menuId']);
                        validate($v, 'SystemRoleValidator', ['pseudoChecked']);
                }
                return jsonResponse(SystemRoleBusiness::setRolePermissions($roleId));
        }
}