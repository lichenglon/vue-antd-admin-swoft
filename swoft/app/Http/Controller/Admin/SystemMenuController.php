<?php declare(strict_types=1);

namespace App\Http\Controller\Admin;

use Swoft\Http\Server\Annotation\Mapping\Controller;
use Swoft\Http\Server\Annotation\Mapping\RequestMapping;
use Swoft\Http\Server\Annotation\Mapping\RequestMethod;
use Swoft\Http\Message\Response;
use Swoft\Validator\Annotation\Mapping\Validate;
use Swoft\Validator\Annotation\Mapping\ValidateType;
use Swoft\Http\Server\Annotation\Mapping\Middleware;
use App\Http\Middleware\AdminLoginMiddleware;
use App\Http\Middleware\AdminAucMiddleware;
use App\Http\Business\Admin\SystemMenuBusiness;

/**
 * 菜单-控制器
 * Class SystemMenuController
 * @package App\Http\Controller\Admin
 * @Controller(prefix="/admin")
 * @Middleware(AdminLoginMiddleware::class)
 * @Middleware(AdminAucMiddleware::class)
 */
class SystemMenuController
{
        /**
         * 查询菜单
         * @RequestMapping(route="menu[/{menuId}]", method={RequestMethod::GET})
         * @param string $menuId
         * @return Response
         */
        public function query(string $menuId): Response
        {
                return jsonResponse(SystemMenuBusiness::query($menuId));
        }

        /**
         * 添加菜单
         * @RequestMapping(route="menu", method={RequestMethod::POST})
         * @Validate(validator="SystemMenuValidator", fields={"pid", "name", "icon", "route", "path", "urlParams", "pathParams", "menuType", "menuType", "status", "method", "powerDiscern", "sort"}, type=ValidateType::BODY)
         * @return Response
         */
        public function add(): Response
        {
                return jsonResponse(SystemMenuBusiness::add());
        }

        /**
         * 编辑菜单
         * @RequestMapping(route="menu/{menuId}[/{field}]", method={RequestMethod::PUT})
         * @Validate(validator="SystemMenuValidator", fields={"menuId"}, type=ValidateType::PATH)
         * @param string $menuId
         * @return Response
         * @throws \Exception
         */
        public function edit(string $menuId, string $field): Response
        {
                if ($field) {
                        validate(post(), 'SystemMenuValidator', [$field]);
                        return jsonResponse(SystemMenuBusiness::editSpecifiedField($menuId, $field));
                }
                validate(post(), 'SystemMenuValidator', ['pid', 'name', 'icon', 'route', 'path', 'urlParams', 'pathParams', 'menuType', 'menuType', 'status', 'method', 'powerDiscern', 'sort']);
                return jsonResponse(SystemMenuBusiness::edit($menuId));
        }

        /**
         * 删除菜单
         * @RequestMapping(route="menu/{menuId}", method={RequestMethod::DELETE})
         * @Validate(validator="SystemMenuValidator", fields={"menuId"}, type=ValidateType::PATH)
         * @return Response
         */
        public function delete(string $menuId): Response
        {
                return jsonResponse(SystemMenuBusiness::delete($menuId));
        }
}