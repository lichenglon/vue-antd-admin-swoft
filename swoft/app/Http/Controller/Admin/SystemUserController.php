<?php declare(strict_types=1);

namespace App\Http\Controller\Admin;

use Swoft\Http\Server\Annotation\Mapping\Controller;
use Swoft\Http\Server\Annotation\Mapping\RequestMapping;
use Swoft\Http\Server\Annotation\Mapping\RequestMethod;
use Swoft\Http\Message\Response;
use App\Http\Business\Admin\SystemUserBusiness;
use Swoft\Validator\Annotation\Mapping\Validate;
use Swoft\Validator\Annotation\Mapping\ValidateType;
use Swoft\Http\Server\Annotation\Mapping\Middleware;
use App\Http\Middleware\AdminLoginMiddleware;
use App\Http\Middleware\AdminAucMiddleware;

/**
 * 系统用户-控制器
 * Class LoginController
 * @package App\Http\Controller\Admin
 * @Controller(prefix="/admin")
 */
class SystemUserController
{
        /**
         * 登录
         * @RequestMapping(route="login", method={RequestMethod::POST})
         * @Validate(validator="SystemUserValidator", fields={"username", "password", "captcha"}, type=ValidateType::BODY)
         * @return Response
         */
        public function login(): Response
        {
                return jsonResponse(SystemUserBusiness::login());
        }

        /**
         * 查询用户
         * @RequestMapping(route="user[/{userId}]", method={RequestMethod::GET})
         * @Middleware(AdminLoginMiddleware::class)
         * @Middleware(AdminAucMiddleware::class)
         * @param string $userId
         * @return Response
         */
        public function query(string $userId): Response
        {
                return jsonResponse(SystemUserBusiness::query($userId));
        }

        /**
         * 新增用户
         * @RequestMapping(route="user", method={RequestMethod::POST})
         * @Validate(validator="SystemUserValidator", fields={"realName", "phone", "username", "password", "status", "avatar", "roleIds"}, type=ValidateType::BODY)
         * @Middleware(AdminLoginMiddleware::class)
         * @Middleware(AdminAucMiddleware::class)
         * @return Response
         */
        public function add(): Response
        {
                return jsonResponse(SystemUserBusiness::add());
        }

        /**
         * 编辑用户
         * @RequestMapping(route="user/{userId}[/{field}]", method={RequestMethod::PUT})
         * @Validate(validator="SystemUserValidator", fields={"userId"}, type=ValidateType::PATH)
         * @Middleware(AdminLoginMiddleware::class)
         * @Middleware(AdminAucMiddleware::class)
         * @param string $userId
         * @param string $field 指定编辑字段
         * @return Response
         * @throws \Exception
         */
        public function edit(string $userId, string $field): Response
        {
                if ($field) {
                        validate(post(), 'SystemUserValidator', [$field]);
                        return jsonResponse(SystemUserBusiness::editSpecifiedField($userId, $field));
                }
                validate(post(), 'SystemUserValidator', ["realName", "phone", "username", "status", "avatar", "roleIds"]);
                return jsonResponse(SystemUserBusiness::edit($userId));
        }
}