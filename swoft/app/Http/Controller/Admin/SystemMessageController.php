<?php declare(strict_types=1);

namespace App\Http\Controller\Admin;

use Swoft\Http\Server\Annotation\Mapping\Controller;
use Swoft\Http\Server\Annotation\Mapping\RequestMapping;
use Swoft\Http\Server\Annotation\Mapping\RequestMethod;
use Swoft\Http\Message\Response;
use Swoft\Validator\Annotation\Mapping\Validate;
use Swoft\Validator\Annotation\Mapping\ValidateType;
use Swoft\Http\Server\Annotation\Mapping\Middleware;
use App\Http\Middleware\AdminLoginMiddleware;
use App\Http\Middleware\AdminAucMiddleware;
use App\Http\Business\Admin\SystemMessageBusiness;

/**
 * 消息通知待办-控制器
 * Class SystemMessageController
 * @package App\Http\Controller\Admin
 * @Controller(prefix="/admin")
 * @Middleware(AdminLoginMiddleware::class)
 * @Middleware(AdminAucMiddleware::class)
 */
class SystemMessageController
{
        /**
         * 查询消息
         * @RequestMapping(route="message[/{messageId}]", method={RequestMethod::GET})
         * @param string $messageId
         * @return Response
         */
        public function query(string $messageId): Response
        {
                return jsonResponse(SystemMessageBusiness::query($messageId));
        }

        /**
         * 新增消息
         * @RequestMapping(route="message", method={RequestMethod::POST})
         * @Validate(validator="SystemMessageValidator", fields={"title", "content", "userIds", "type"}, type=ValidateType::BODY)
         * @return Response
         */
        public function add(): Response
        {
                return jsonResponse(SystemMessageBusiness::add());
        }

        /**
         * 删除消息
         * @RequestMapping(route="message/{messageId}", method={RequestMethod::DELETE})
         * @Validate(validator="SystemMessageValidator", fields={"messageId"}, type=ValidateType::PATH)
         * @param string $messageId
         * @return Response
         */
        public function delete(string $messageId): Response
        {
                return jsonResponse(SystemMessageBusiness::delete($messageId));
        }
}