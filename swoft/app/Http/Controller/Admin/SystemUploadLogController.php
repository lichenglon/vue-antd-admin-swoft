<?php declare(strict_types=1);

namespace App\Http\Controller\Admin;

use Swoft\Http\Server\Annotation\Mapping\Controller;
use Swoft\Http\Server\Annotation\Mapping\RequestMapping;
use Swoft\Http\Server\Annotation\Mapping\RequestMethod;
use Swoft\Http\Message\Response;
use Swoft\Validator\Annotation\Mapping\Validate;
use Swoft\Validator\Annotation\Mapping\ValidateType;
use Swoft\Http\Server\Annotation\Mapping\Middleware;
use App\Http\Middleware\AdminLoginMiddleware;
use App\Http\Business\Admin\SystemUploadLogBusiness;

/**
 * 系统上传文件记录-控制器
 * Class SystemUploadLogController
 * @package App\Http\Controller\Admin
 * @Controller(prefix="/admin")
 * @Middleware(AdminLoginMiddleware::class)
 */
class SystemUploadLogController
{
        /**
         * 上传图片
         * @RequestMapping(route="uploadPic", method={RequestMethod::POST})
         * @return Response
         */
        public function uploadPic(): Response
        {
                return jsonResponse(SystemUploadLogBusiness::uploadPic(1));
        }
}