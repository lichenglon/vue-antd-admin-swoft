<?php declare(strict_types=1);

namespace App\Http\Controller;

use Swoft\Http\Server\Annotation\Mapping\Controller;
use Swoft\Http\Server\Annotation\Mapping\RequestMapping;
use Swoft\Http\Server\Annotation\Mapping\RequestMethod;
use Swoft\Validator\Annotation\Mapping\Validate;
use Swoft\Validator\Annotation\Mapping\ValidateType;
use Swoft\Http\Server\Annotation\Mapping\Middleware;
use App\Http\Middleware\AdminLoginMiddleware;
use Swoft\Http\Message\Response;
use App\Http\Business\CommonBusiness;


/**
 * 公共-控制器
 * Class CommonController
 * @package App\Http\Controller
 * @Controller(prefix="/common")
 */
class CommonController
{
        /**
         * 图片验证码
         * @RequestMapping(route="captcha", method={RequestMethod::GET})
         * @return Response
         */
        public function captcha(): Response
        {
                return jsonResponse(CommonBusiness::captcha());
        }

        /**
         * 获取当前登录用户路由权限
         * @RequestMapping(route="routes", method={RequestMethod::GET})
         * @Middleware(AdminLoginMiddleware::class)
         * @return Response
         */
        public function routes(): Response
        {
                return jsonResponse(CommonBusiness::routes());
        }

        /**
         * 个人设置
         * 获取显示数据
         * @RequestMapping(route="personalSetting", method={RequestMethod::GET})
         * @Middleware(AdminLoginMiddleware::class)
         * @return Response
         */
        public function personalSettingInfo(): Response
        {
                return jsonResponse(CommonBusiness::personalSettingInfo());
        }

        /**
         * 个人设置
         * 编辑个人信息
         * @RequestMapping(route="personalSetting", method={RequestMethod::PUT})
         * @Middleware(AdminLoginMiddleware::class)
         * @Validate(validator="SystemUserValidator", fields={"realName", "phone", "avatar"}, type=ValidateType::BODY)
         * @return Response
         */
        public function personalSettingEdit(): Response
        {
                return jsonResponse(CommonBusiness::personalSettingEdit());
        }
}