<?php
declare(strict_types=1);

namespace App\Http\Controller;

use Swoft\Http\Server\Annotation\Mapping\Controller;
use Swoft\Http\Server\Annotation\Mapping\RequestMapping;
use Swoft\Http\Server\Annotation\Mapping\RequestMethod;
use Swoft\Http\Message\Response;
use App\Model\Master\Member\Member as MasterMember;
use Swoft\Redis\Redis;

/**
 * 测试控制器
 * Class PhysicalTestController
 * @Controller(prefix="/test")
 */
class TestController
{
        /**
         * 获取体测分布
         * @RequestMapping(route="index", method={RequestMethod::GET, RequestMethod::POST})
         * @return Response
         */
        public function index(): Response
        {
                /*$intPage = 2;
                $intLimit = 10;
                $arrWhere = [];
                $arrField = ['memberId', 'realName', 'phone', 'status', 'lastTime', 'lastIp', 'inTime'];
                $arrResult = MasterMember::getList($arrWhere, $arrField, $intPage, $intLimit);*/
                /*$arrWhere = ['phone' => '13267166502'];
                $arrField = ['memberId', 'realName', 'phone', 'status', 'lastTime', 'lastIp', 'inTime'];
                $arrResult = MasterMember::getOne($arrWhere, $arrField);
                $arrWhere = [
                        'memberId' => $arrResult['memberId'],
                ];
                $arrSet = [
                        'lastTime' => time(),
                        'lastIp' => $this->randIp()
                ];
                $bool = MasterMember::renewal($arrWhere, $arrSet);*/
                //Redis::set('test', 2);
                var_export(post());
                return jsonResponse(post());
        }

        /**
         * GET方式查询一项或多项会员
         * @RequestMapping(route="member", method={RequestMethod::GET})
         * @return Response
         */
        public function queryMember(): Response
        {
                $arrParam = get();
                $intPage = intval(issetArrKey($arrParam, 'page', 1));
                $intLimit = intval(issetArrKey($arrParam, 'limit', 10));
                $strRealName = strAddslashes(issetArrKey($arrParam, 'realName', ''));
                $strPhone = strAddslashes(issetArrKey($arrParam, 'phone', ''));
                $arrWhere = [];
                if ($strRealName) $arrWhere[] = ['realName', 'like', "%{$strRealName}%"];
                //if ($strPhone) $arrWhere[] = ['phone', 'like', "%{$strPhone}%"];
                if ($strPhone) $arrWhere['phone'] = $strPhone;
                $arrField = ['memberId', 'realName', 'phone', 'status', 'lastTime', 'lastIp', 'inTime'];
                $arrOrderBy = [];
                $arrResult = MasterMember::getLimit($arrWhere, $arrField, $intPage, $intLimit, $arrOrderBy);
                return jsonResponse(['status' => 0, 'data' => $arrResult, 'msg' => 'SUCCESS']);
        }

        /**
         * POST方式创建会员
         * @RequestMapping(route="member", method={RequestMethod::POST})
         * @return Response
         */
        public function createMember(): Response
        {
                $arrParam = post();
                $arrSet = [
                        'memberId' => uuid(),
                        'realName' => $arrParam['realName'],
                        'phone' => $arrParam['phone'],
                        'lastTime' => time(),
                        'lastIp' => $this->randIp(),
                        'inTime' => time()
                ];
                $bool = MasterMember::preserve($arrSet);
                return jsonResponse(['status' => 0, 'data' => $bool, 'msg' => 'SUCCESS']);
        }

        /**
         * PUT方式更新会员
         * @RequestMapping(route="member[/{memberId}]", method={RequestMethod::PUT})
         * @return Response
         */
        public function updateMember(string $memberId): Response
        {
                $arrParam = post();
                $arrWhere = [
                        'memberId' => $memberId
                ];
                $arrSet = [
                        'realName' => $arrParam['realName'],
                        'phone' => $arrParam['phone'],
                ];
                $bool = MasterMember::renewal($arrWhere, $arrSet);
                return jsonResponse(['status' => 0, 'data' => $memberId, 'msg' => 'SUCCESS']);
        }

        /**
         * DELETE方式删除会员
         * @RequestMapping(route="member[/{memberId}]", method={RequestMethod::DELETE})
         * @return Response
         */
        public function deleteMember(string $memberId): Response
        {
                return jsonResponse(['status' => 0, 'data' => $memberId, 'msg' => 'SUCCESS']);
        }


        /**
         * 随机生成IP
         * @return string
         */
        protected function randIp()
        {
                $arrIpAll = array(
                        array(array(58, 14), array(58, 25)),
                        array(array(58, 30), array(58, 63)),
                        array(array(58, 66), array(58, 67)),
                        array(array(60, 200), array(60, 204)),
                        array(array(60, 160), array(60, 191)),
                        array(array(60, 208), array(60, 223)),
                        array(array(117, 48), array(117, 51)),
                        array(array(117, 57), array(117, 57)),
                        array(array(121, 8), array(121, 29)),
                        array(array(121, 192), array(121, 199)),
                        array(array(123, 144), array(123, 149)),
                        array(array(124, 112), array(124, 119)),
                        array(array(125, 64), array(125, 98)),
                        array(array(222, 128), array(222, 143)),
                        array(array(222, 160), array(222, 163)),
                        array(array(220, 248), array(220, 252)),
                        array(array(211, 163), array(211, 163)),
                        array(array(210, 21), array(210, 22)),
                        array(array(125, 32), array(125, 47))
                );
                $ipRandIndex = mt_rand(0, count($arrIpAll) - 1);#随机生成需要IP段
                $ip1 = $arrIpAll[$ipRandIndex][0][0];
                if ($arrIpAll[$ipRandIndex][0][1] == $arrIpAll[$ipRandIndex][1][1]) {
                        $ip2 = $arrIpAll[$ipRandIndex][0][1];
                } else {
                        $ip2 = mt_rand(intval($arrIpAll[$ipRandIndex][0][1]), intval($arrIpAll[$ipRandIndex][1][1]));
                }
                $ip3 = mt_rand(0, 255);
                $ip4 = mt_rand(0, 255);
                $member = null;
                $arrIpAll = null;
                return $ip1 . '.' . $ip2 . '.' . $ip3 . '.' . $ip4;
        }
}



