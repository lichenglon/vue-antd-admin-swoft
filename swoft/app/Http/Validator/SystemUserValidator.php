<?php declare(strict_types=1);

namespace App\Http\Validator;

use Swoft\Validator\Annotation\Mapping\Enum;
use Swoft\Validator\Annotation\Mapping\IsArray;
use Swoft\Validator\Annotation\Mapping\IsInt;
use Swoft\Validator\Annotation\Mapping\IsString;
use Swoft\Validator\Annotation\Mapping\Length;
use Swoft\Validator\Annotation\Mapping\Mobile;
use Swoft\Validator\Annotation\Mapping\NotEmpty;
use Swoft\Validator\Annotation\Mapping\Required;
use Swoft\Validator\Annotation\Mapping\Validator;

/**
 * 系统用户-验证器
 * Class AdminUserValidator
 * @package App\Http\Validator
 * @Validator(name="SystemUserValidator")
 */
class SystemUserValidator
{
        /**
         * @Required()
         * @NotEmpty()
         * @IsString()
         * @Length(min=39, max=39)
         * @var string
         */
        protected $userId;

        /**
         * @Required()
         * @NotEmpty()
         * @IsString()
         * @var string
         */
        protected $avatar;

        /**
         * @Required()
         * @NotEmpty()
         * @IsString()
         * @var string
         */
        protected $realName;

        /**
         * @Required()
         * @NotEmpty()
         * @IsString()
         * @Mobile()
         * @var string
         */
        protected $phone;

        /**
         * @Required()
         * @NotEmpty()
         * @IsString()
         * @Length(min=3, max=30)
         * @var string
         */
        protected $username;

        /**
         * @Required()
         * @NotEmpty()
         * @IsString()
         * @Length(min=3, max=30)
         * @var string
         */
        protected $password;

        /**
         * @Required()
         * @NotEmpty()
         * @IsInt()
         * @Enum(values={1, 2})
         * @var int
         */
        protected $status;

        /**
         * @Required()
         * @IsString()
         * @var float
         */
        protected $captcha;

        /**
         * @Required()
         * @NotEmpty()
         * @IsArray()
         * @var array
         */
        protected $roleIds;
}