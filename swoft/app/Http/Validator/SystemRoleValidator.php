<?php declare(strict_types=1);

namespace App\Http\Validator;

use Swoft\Validator\Annotation\Mapping\Enum;
use Swoft\Validator\Annotation\Mapping\IsArray;
use Swoft\Validator\Annotation\Mapping\IsInt;
use Swoft\Validator\Annotation\Mapping\IsString;
use Swoft\Validator\Annotation\Mapping\Length;
use Swoft\Validator\Annotation\Mapping\Max;
use Swoft\Validator\Annotation\Mapping\Min;
use Swoft\Validator\Annotation\Mapping\NotEmpty;
use Swoft\Validator\Annotation\Mapping\Required;
use Swoft\Validator\Annotation\Mapping\Validator;

/**
 * 系统角色-验证器
 * Class SystemRoleValidator
 * @package App\Http\Validator
 * @Validator(name="SystemRoleValidator")
 */
class SystemRoleValidator
{
        /**
         * @Required()
         * @NotEmpty()
         * @IsString()
         * @Length(min=39, max=39)
         * @var string
         */
        protected $roleId;

        /**
         * @Required()
         * @NotEmpty()
         * @IsString()
         * @Length(min=1, max=20)
         * @var string
         */
        protected $name;

        /**
         * @IsInt()
         * @Min(value=1)
         * @Max(value=100000000)
         * @var int
         */
        protected $sort;

        /**
         * @Required()
         * @IsInt()
         * @Enum(values={1, 2})
         * @var int
         */
        protected $status;

        /**
         * @Required()
         * @IsArray()
         * @var array
         */
        protected $permissionsData;

        /**
         * @Required()
         * @IsInt()
         * @Enum(values={1, 2})
         * @var int
         */
        protected $pseudoChecked;
}