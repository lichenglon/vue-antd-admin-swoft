<?php declare(strict_types=1);

namespace App\Http\Validator;

use Swoft\Validator\Annotation\Mapping\Enum;
use Swoft\Validator\Annotation\Mapping\IsArray;
use Swoft\Validator\Annotation\Mapping\IsInt;
use Swoft\Validator\Annotation\Mapping\IsString;
use Swoft\Validator\Annotation\Mapping\Length;
use Swoft\Validator\Annotation\Mapping\NotEmpty;
use Swoft\Validator\Annotation\Mapping\Required;
use Swoft\Validator\Annotation\Mapping\Validator;

/**
 * 消息通知待办-验证器
 * Class SystemMessageValidator
 * @package App\Http\Validator
 * @Validator(name="SystemMessageValidator")
 */
class SystemMessageValidator
{
        /**
         * @Required()
         * @NotEmpty()
         * @IsString()
         * @Length(min=39, max=39)
         * @var string
         */
        protected $messageId;

        /**
         * @Required()
         * @NotEmpty()
         * @IsString()
         * @Length(max=50)
         * @var string
         */
        protected $title;

        /**
         * @Required()
         * @NotEmpty()
         * @IsString()
         * @Length(max=255)
         * @var string
         */
        protected $content;

        /**
         * @Required()
         * @IsInt()
         * @Enum(values={1, 2, 3})
         * @var int
         */
        protected $type;

        /**
         * @Required()
         * @NotEmpty()
         * @IsArray()
         * @var array
         */
        protected $userIds;
}