<?php declare(strict_types=1);

namespace App\Http\Validator;

use Swoft\Validator\Annotation\Mapping\Enum;
use Swoft\Validator\Annotation\Mapping\IsArray;
use Swoft\Validator\Annotation\Mapping\IsInt;
use Swoft\Validator\Annotation\Mapping\IsString;
use Swoft\Validator\Annotation\Mapping\Length;
use Swoft\Validator\Annotation\Mapping\Max;
use Swoft\Validator\Annotation\Mapping\Min;
use Swoft\Validator\Annotation\Mapping\NotEmpty;
use Swoft\Validator\Annotation\Mapping\Required;
use Swoft\Validator\Annotation\Mapping\Validator;

/**
 * 系统菜单-验证器
 * Class SystemMenuValidator
 * @package App\Http\Validator
 * @Validator(name="SystemMenuValidator")
 */
class SystemMenuValidator
{
        /**
         * @Required()
         * @NotEmpty()
         * @IsString()
         * @Length(min=39, max=39)
         * @var string
         */
        protected $menuId;

        /**
         * @NotEmpty()
         * @IsString()
         * @Length(min=39, max=39)
         * @var string
         */
        protected $pid;

        /**
         * @Required()
         * @NotEmpty()
         * @IsString()
         * @Length(min=1, max=50)
         * @var string
         */
        protected $name;

        /**
         * @NotEmpty()
         * @IsString()
         * @Length(max=20)
         * @var string
         */
        protected $icon;

        /**
         * @NotEmpty()
         * @IsString()
         * @Length(min=1, max=100)
         * @var string
         */
        protected $route;

        /**
         * @NotEmpty()
         * @IsString()
         * @Length(min=1, max=100)
         * @var string
         */
        protected $path;

        /**
         * @IsArray()
         * @var string
         */
        protected $urlParams;

        /**
         * @IsArray()
         * @var string
         */
        protected $pathParams;

        /**
         * @Required()
         * @IsInt()
         * @Enum(values={1, 2})
         * @var int
         */
        protected $menuType;

        /**
         * @Required()
         * @IsInt()
         * @Enum(values={1, 2})
         * @var int
         */
        protected $openType;

        /**
         * @Required()
         * @IsInt()
         * @Enum(values={1, 2})
         * @var int
         */
        protected $status;

        /**
         * @Required()
         * @NotEmpty()
         * @IsString()
         * @Enum(values={"get", "post", "put", "delete", "all"})
         * @var string
         */
        protected $method;

        /**
         * @Required()
         * @NotEmpty()
         * @IsString()
         * @Length(min=1, max=100)
         * @var string
         */
        protected $powerDiscern;

        /**
         * @IsInt()
         * @Min(value=1)
         * @Max(value=100000000)
         * @var int
         */
        protected $sort;
}