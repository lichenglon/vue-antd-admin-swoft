<?php declare(strict_types=1);

namespace App\Task\Task;

use Swoft\Task\Annotation\Mapping\Task;
use Swoft\Task\Annotation\Mapping\TaskMapping;
use App\Model\Task\ExportTask as ExportTaskModel;

/**
 * 导出任务
 * Class ExportTask
 * @Task(name="ExportTask")
 */
class ExportTask
{
        /**
         * 创建任务
         * @TaskMapping(name="makeTask")
         * @param array $arrSetData
         * @return array
         */
        public function makeTask(array $arrSetData): array
        {
                $boole = ExportTaskModel::insert($arrSetData);
                if (!$boole) ['code' => 304, 'msg' => '导出任务创建失败'];
                return [
                        'code' => 0,
                        'msg' => '导出任务创建成功，请到导出结果中心下载',
                        'data' => [
                                'exportTaskId' => $arrSetData['exportTaskId']
                        ]
                ];
        }

        /**
         * 执行任务
         * @TaskMapping(name="execTask")
         * @param string $strExportTaskId
         * @return array
         */
        public function execTask(string $strExportTaskId): array
        {
                $arrWhere = ['exportTaskId' => $strExportTaskId];
                $arrField = ['exportTaskId', 'userInfo', 'name', 'where', 'source', 'type', 'inTime'];
                $arrExportTask = ExportTaskModel::getOne($arrWhere, $arrField);
                switch ($arrExportTask['type']) {
                        case 1:
                                return $this->kidExport($arrExportTask, $strExportTaskId);
                                break;
                        default :
                                return ['code' => 0, 'msg' => '导出任务处理失败，未知的导出任务类型'];
                }
        }

        /**
         * 小孩导出
         * @param array $arrExportTask
         * @param string $strExportTaskId
         * @return array
         */
        protected function kidExport(array $arrExportTask, string $strExportTaskId): array
        {
                $arrWhere = ['exportTaskId' => $strExportTaskId];
                try {
                        $arrJoin = [
                                'kid_institution' => [
                                        'table' => 'kid_institution',
                                        'where' => ['kid_institution.kidId', '=', 'kid.kidId']
                                ]
                        ];
                        $arrOrderBy = [];
                        $arrGroupBy = [];
                        $arrField = ['kid.serialNumber', 'kid.idCard', 'kid.realname', 'kid.sex', 'kid.birthday', 'kid.classesId', 'kid.parentsPhone', 'kid.sort', 'kid.inTime'];
                        $arrList = \App\Model\Kid\Kid::getAll(json_decode($arrExportTask['where'], true), $arrField, $arrOrderBy, $arrGroupBy, $arrJoin);
                        //$objExcel = new \App\Helper\Lib\Excel();
                        $arrData = [
                                'header' => ['小孩编号', '身份证号', '姓名', '性别', '生日', '班级', '家长手机号', '创建时间'],
                        ];
                        $arrContent = [];
                        $arrClasses = \App\Model\Classes\Classes::getColumn([], 'name', [], 'classesId');
                        foreach ($arrList as $v) {
                                $arrContent[] = [
                                        $v['serialNumber'],
                                        $v['idCard'] . "\t",
                                        $v['realname'],
                                        $v['sex'],
                                        date('Y-m-d', $v['birthday']),
                                        issetArrKey($arrClasses, $v['classesId'], ''),
                                        $v['parentsPhone'],
                                        date('Y-m-d H:i:s', $v['inTime'])
                                ];
                        }
                        $arrData['content'] = $arrContent;
                        $strFilePath = '/public/uploads/file/export/' . $strExportTaskId . '.xlsx';
                        $boole = \App\Helper\Lib\Excel::saveExportExcel($arrData, $strFilePath);
                        if ($boole) {
                                $arrSet = ['status' => 2, 'filePath' => $strFilePath];
                        } else {
                                $arrSet = ['status' => 3];
                        }
                        ExportTaskModel::renewal($arrWhere, $arrSet);
                        unset($objExcel);
                        return [
                                'code' => 0,
                                'msg' => '导出任务处理成功，请到导出结果中心下载',
                                'data' => [
                                        'exportTaskId' => $strExportTaskId
                                ]
                        ];
                } catch (Exception $e) {
                        $arrSet = ['status' => 3];
                        ExportTaskModel::renewal($arrWhere, $arrSet);
                        return [
                                'code' => 304,
                                'msg' => '导出任务处理失败，' . $e->getMessage(),
                        ];
                }
        }
}