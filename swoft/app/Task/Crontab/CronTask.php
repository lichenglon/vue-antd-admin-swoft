<?php declare(strict_types=1);
/**
 * This file is part of Swoft.
 *
 * @link     https://swoft.org
 * @document https://swoft.org/docs
 * @contact  group@swoft.org
 * @license  https://github.com/swoft-cloud/swoft/blob/master/LICENSE
 */

namespace App\Task\Crontab;

use App\Model\Master\Member\Member as MasterMember;
use Exception;
use Swoft\Crontab\Annotaion\Mapping\Cron;
use Swoft\Crontab\Annotaion\Mapping\Scheduled;
use Swoft\Log\Helper\CLog;

/**
 * Class CronTask
 *
 * @since 2.0
 *
 * @Scheduled()
 */
class CronTask
{
        /**
         * @Cron("* * * * * *")
         *
         * @throws Exception
         */
        public function secondTask(): void
        {

                $arrSurnames = array(
                        0 => '赵', 1 => '钱', 2 => '孙', 3 => '李', 4 => '周', 5 => '吴', 6 => '郑', 7 => '王',
                        8 => '冯', 9 => '陈', 10 => '褚', 11 => '卫', 12 => '蒋', 13 => '沈', 14 => '韩',
                        15 => '杨', 16 => '朱', 17 => '秦', 18 => '尤', 19 => '许', 20 => '何', 21 => '吕',
                        22 => '施', 23 => '张', 24 => '孔', 25 => '曹', 26 => '严', 27 => '华', 28 => '金',
                        29 => '魏', 30 => '陶', 31 => '姜', 32 => '戚', 33 => '谢', 34 => '邹', 35 => '喻',
                        36 => '柏', 37 => '水', 38 => '窦', 39 => '章', 40 => '云', 41 => '苏', 42 => '潘',
                        43 => '葛', 44 => '奚', 45 => '范', 46 => '彭', 47 => '郎', 48 => '鲁', 49 => '韦',
                        50 => '昌', 51 => '马', 52 => '苗', 53 => '凤', 54 => '花', 55 => '方', 56 => '俞',
                        57 => '任', 58 => '袁', 59 => '柳', 60 => '酆', 61 => '鲍', 62 => '史', 63 => '唐',
                        64 => '费', 65 => '廉', 66 => '岑', 67 => '薛', 68 => '雷', 69 => '贺', 70 => '倪',
                        71 => '汤', 72 => '滕', 73 => '殷', 74 => '罗', 75 => '毕', 76 => '郝', 77 => '邬',
                        78 => '安', 79 => '常', 80 => '乐', 81 => '于', 82 => '时', 83 => '傅', 84 => '皮',
                        85 => '卞', 86 => '齐', 87 => '康', 88 => '伍', 89 => '余', 90 => '元', 91 => '卜',
                        92 => '顾', 93 => '孟', 94 => '平', 95 => '黄', 96 => '和', 97 => '穆', 98 => '萧',
                        99 => '尹', 100 => '姚', 101 => '邵', 102 => '湛', 103 => '汪', 104 => '祁', 105 => '毛',
                        106 => '禹', 107 => '狄', 108 => '米', 109 => '贝', 110 => '明', 111 => '臧', 112 => '计',
                        113 => '伏', 114 => '成', 115 => '戴', 116 => '谈', 117 => '宋', 118 => '茅', 119 => '庞',
                        120 => '熊', 121 => '纪', 122 => '舒', 123 => '屈', 124 => '项', 125 => '祝', 126 => '董',
                        127 => '梁', 128 => '杜', 129 => '阮', 130 => '蓝', 131 => '闵', 132 => '席', 133 => '季',
                        134 => '麻', 135 => '强', 136 => '贾', 137 => '路', 138 => '娄', 139 => '危', 140 => '江',
                        141 => '童', 142 => '颜', 143 => '郭', 144 => '贾', 145 => '路', 146 => '娄', 147 => '危',
                        148 => '江', 149 => '童', 150 => '颜', 151 => '郭', 152 => '高', 153 => '夏', 154 => '蔡',
                        155 => '田', 156 => '樊', 157 => '胡', 158 => '凌', 159 => '霍',
                        160 => '虞',
                        161 => '万',
                        162 => '支',
                        163 => '柯',
                        164 => '昝',
                        165 => '管',
                        166 => '卢',
                        167 => '莫',
                        168 => '经',
                        169 => '房',
                        170 => '裘',
                        171 => '缪',
                        172 => '干',
                        173 => '解',
                        174 => '应',
                        175 => '宗',
                        176 => '丁',
                        177 => '宣',
                        178 => '贲',
                        179 => '邓',
                        180 => '郁',
                        181 => '单',
                        182 => '杭',
                        183 => '洪',
                        184 => '包',
                        185 => '诸',
                        186 => '左',
                        187 => '石',
                        188 => '崔',
                        189 => '吉',
                        190 => '钮',
                        191 => '龚',
                        192 => '程',
                        193 => '嵇',
                        194 => '邢',
                        195 => '滑',
                        196 => '裴',
                        197 => '陆',
                        198 => '荣',
                        199 => '翁',
                        200 => '荀',
                        201 => '羊',
                        202 => '於',
                        203 => '惠',
                        204 => '甄',
                        205 => '曲',
                        206 => '家',
                        207 => '封',
                        208 => '芮',
                        209 => '羿',
                        210 => '储',
                        211 => '靳',
                        212 => '汲',
                        213 => '邴',
                        214 => '糜',
                        215 => '松',
                        216 => '井',
                        217 => '段',
                        218 => '富',
                        219 => '巫',
                        220 => '乌',
                        221 => '焦',
                        222 => '巴',
                        223 => '弓',
                        224 => '牧',
                        225 => '隗',
                        226 => '山',
                        227 => '谷',
                        228 => '车',
                        229 => '侯',
                        230 => '宓',
                        231 => '蓬',
                        232 => '全',
                        233 => '郗',
                        234 => '班',
                        235 => '仰',
                        236 => '秋',
                        237 => '仲',
                        238 => '伊',
                        239 => '宫',
                        240 => '宁',
                        241 => '仇',
                        242 => '栾',
                        243 => '暴',
                        244 => '甘',
                        245 => '钭',
                        246 => '厉',
                        247 => '戎',
                        248 => '祖',
                        249 => '武',
                        250 => '符',
                        251 => '刘',
                        252 => '景',
                        253 => '詹',
                        254 => '束',
                        255 => '龙',
                        256 => '叶',
                        257 => '幸',
                        258 => '司',
                        259 => '韶',
                        260 => '郜',
                        261 => '黎',
                        262 => '蓟',
                        263 => '薄',
                        264 => '印',
                        265 => '宿',
                        266 => '白',
                        267 => '怀',
                        268 => '蒲',
                        269 => '邰',
                        270 => '从',
                        271 => '鄂',
                        272 => '索',
                        273 => '咸',
                        274 => '籍',
                        275 => '赖',
                        276 => '卓',
                        277 => '蔺',
                        278 => '屠',
                        279 => '蒙',
                        280 => '池',
                        281 => '乔',
                        282 => '阴',
                        283 => '鬱',
                        284 => '胥',
                        285 => '能',
                        286 => '苍',
                        287 => '双',
                        288 => '闻',
                        289 => '莘',
                        290 => '党',
                        291 => '翟',
                        292 => '谭',
                        293 => '贡',
                        294 => '劳',
                        295 => '逄',
                        296 => '姬',
                        297 => '申',
                        298 => '扶',
                        299 => '堵',
                        300 => '冉',
                        301 => '宰',
                        302 => '郦',
                        303 => '雍',
                        304 => '卻',
                        305 => '璩',
                        306 => '桑',
                        307 => '桂',
                        308 => '濮',
                        309 => '牛',
                        310 => '寿',
                        311 => '通',
                        312 => '边',
                        313 => '扈',
                        314 => '燕',
                        315 => '冀',
                        316 => '郏',
                        317 => '浦',
                        318 => '尚',
                        319 => '农',
                        320 => '温',
                        321 => '别',
                        322 => '庄',
                        323 => '晏',
                        324 => '柴',
                        325 => '瞿',
                        326 => '阎',
                        327 => '充',
                        328 => '慕',
                        329 => '连',
                        330 => '茹',
                        331 => '习',
                        332 => '宦',
                        333 => '艾',
                        334 => '鱼',
                        335 => '容',
                        336 => '向',
                        337 => '古',
                        338 => '易',
                        339 => '慎',
                        340 => '戈',
                        341 => '廖',
                        342 => '庾',
                        343 => '终',
                        344 => '暨',
                        345 => '居',
                        346 => '衡',
                        347 => '步',
                        348 => '都',
                        349 => '耿',
                        350 => '满',
                        351 => '弘',
                        352 => '匡',
                        353 => '国',
                        354 => '文',
                        355 => '寇',
                        356 => '广',
                        357 => '禄',
                        358 => '阙',
                        359 => '东',
                        360 => '欧',
                        361 => '殳',
                        362 => '沃',
                        363 => '利',
                        364 => '蔚',
                        365 => '越',
                        366 => '夔',
                        367 => '隆',
                        368 => '师',
                        369 => '巩',
                        370 => '厍',
                        371 => '聂',
                        372 => '晁',
                        373 => '勾',
                        374 => '敖',
                        375 => '融',
                        376 => '冷',
                        377 => '訾',
                        378 => '辛',
                        379 => '阚',
                        380 => '那',
                        381 => '简',
                        382 => '饶',
                        383 => '空',
                        384 => '曾',
                        385 => '毋',
                        386 => '沙',
                        387 => '乜',
                        388 => '养',
                        389 => '鞠',
                        390 => '须',
                        391 => '丰',
                        392 => '巢',
                        393 => '关',
                        394 => '蒯',
                        395 => '相',
                        396 => '查',
                        397 => '后',
                        398 => '荆',
                        399 => '红',
                        400 => '游',
                        401 => '竺',
                        402 => '权',
                        403 => '逯',
                        404 => '盖',
                        405 => '益',
                        406 => '桓',
                        407 => '公',
                        408 => '万俟',
                        409 => '司马',
                        410 => '上官',
                        411 => '欧阳',
                        412 => '夏侯',
                        413 => '诸葛',
                        414 => '闻人',
                        415 => '东方',
                        416 => '赫连',
                        417 => '皇甫',
                        418 => '尉迟',
                        419 => '公羊',
                        420 => '澹台',
                        421 => '公冶',
                        422 => '宗政',
                        423 => '濮阳',
                        424 => '淳',
                        425 => '于',
                        426 => '单',
                        427 => '于',
                        428 => '太叔',
                        429 => '申屠',
                        430 => '公孙',
                        431 => '仲孙',
                        432 => '轩辕',
                        433 => '令狐',
                        434 => '钟离',
                        435 => '宇文',
                        436 => '长孙',
                        437 => '慕容',
                        438 => '鲜于',
                        439 => '闾丘',
                        440 => '司徒',
                        441 => '司空',
                        442 => '丌官',
                        443 => '司寇',
                        444 => '仉',
                        445 => '督',
                        446 => '子车',
                        447 => '颛孙',
                        448 => '端木',
                        449 => '巫马',
                        450 => '公西',
                        451 => '漆雕',
                        452 => '乐正',
                        453 => '壤驷',
                        454 => '公良',
                        455 => '拓跋',
                        456 => '夹谷',
                        457 => '宰父',
                        458 => '谷梁',
                        459 => '晋',
                        460 => '楚',
                        461 => '闫',
                        462 => '法',
                        463 => '汝',
                        464 => '鄢',
                        465 => '涂',
                        466 => '钦',
                        467 => '段干',
                        468 => '百里',
                        469 => '东郭',
                        470 => '南门',
                        471 => '呼延',
                        472 => '归海',
                        473 => '羊舌',
                        474 => '微生',
                        475 => '岳',
                        476 => '帅',
                        477 => '缑',
                        478 => '亢',
                        479 => '况',
                        480 => '郈',
                        481 => '有',
                        482 => '琴',
                        483 => '梁丘',
                        484 => '左丘',
                        485 => '东门',
                        486 => '西门',
                        487 => '商',
                        488 => '牟',
                        489 => '佘',
                        490 => '佴',
                        491 => '伯',
                        492 => '赏',
                        493 => '南宫',
                        494 => '墨',
                        495 => '哈',
                        496 => '谯',
                        497 => '笪',
                        498 => '年',
                        499 => '爱',
                        500 => '阳',
                        501 => '佟',
                        502 => '第五',
                        503 => '言',
                        504 => '福',
                );
                shuffle($arrSurnames);
                $arrSet = [];
                $arrNumber = range(1, 9);
                $intNowTime = time();
                for ($i = 0; $i <= 2000; $i++) {
                        $arrRandSurnamesIndex = array_rand($arrSurnames, 3);
                        $strRealName = '';
                        foreach ($arrRandSurnamesIndex as $v) {
                                $strRealName .= $arrSurnames[$v];
                        }
                        $arrRandNumber = array_rand($arrNumber, 9);
                        shuffle($arrRandNumber);
                        $strRandLastPhone = '';
                        foreach ($arrRandNumber as $v) {
                                $strRandLastPhone .= $arrNumber[$v];
                        }
                        $arrSet[] = [
                                'memberId' => uuid(),
                                'realName' => $strRealName,
                                'phone' => '13' . $strRandLastPhone,
                                'status' => 1,
                                'lastTime' => $intNowTime,
                                'lastIp' => $this->randIp(),
                                'inTime' => $intNowTime
                        ];
                }
                $bool = MasterMember::insert($arrSet);
                var_export($bool);
                echo "\n";
                //CLog::info('second task run: %s ', date('Y-m-d H:i:s') . $bool);
        }

        /**
         * 随机生成IP
         * @return string
         */
        protected function randIp()
        {
                $arrIpAll = array(
                        array(array(58, 14), array(58, 25)),
                        array(array(58, 30), array(58, 63)),
                        array(array(58, 66), array(58, 67)),
                        array(array(60, 200), array(60, 204)),
                        array(array(60, 160), array(60, 191)),
                        array(array(60, 208), array(60, 223)),
                        array(array(117, 48), array(117, 51)),
                        array(array(117, 57), array(117, 57)),
                        array(array(121, 8), array(121, 29)),
                        array(array(121, 192), array(121, 199)),
                        array(array(123, 144), array(123, 149)),
                        array(array(124, 112), array(124, 119)),
                        array(array(125, 64), array(125, 98)),
                        array(array(222, 128), array(222, 143)),
                        array(array(222, 160), array(222, 163)),
                        array(array(220, 248), array(220, 252)),
                        array(array(211, 163), array(211, 163)),
                        array(array(210, 21), array(210, 22)),
                        array(array(125, 32), array(125, 47))
                );
                $ipRandIndex = mt_rand(0, count($arrIpAll) - 1);#随机生成需要IP段
                $ip1 = $arrIpAll[$ipRandIndex][0][0];
                if ($arrIpAll[$ipRandIndex][0][1] == $arrIpAll[$ipRandIndex][1][1]) {
                        $ip2 = $arrIpAll[$ipRandIndex][0][1];
                } else {
                        $ip2 = mt_rand(intval($arrIpAll[$ipRandIndex][0][1]), intval($arrIpAll[$ipRandIndex][1][1]));
                }
                $ip3 = mt_rand(0, 255);
                $ip4 = mt_rand(0, 255);
                $member = null;
                $arrIpAll = null;
                return $ip1 . '.' . $ip2 . '.' . $ip3 . '.' . $ip4;
        }
        /**
         * #@Cron("0 * * * * *")
         */
        /*public function minuteTask(): void
        {
            CLog::info('minute task run: %s ', date('Y-m-d H:i:s'));
        }*/
}
