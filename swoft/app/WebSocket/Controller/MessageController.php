<?php declare(strict_types=1);

namespace App\WebSocket\Controller;

use Swoft\Session\Session;
use Swoft\WebSocket\Server\Annotation\Mapping\MessageMapping;
use Swoft\WebSocket\Server\Annotation\Mapping\WsController;
use App\WebSocket\Middleware\DemoMiddleware;
use App\WebSocket\Business\MessageBusiness;

/**
 * Class MessageController
 * @package App\WebSocket\Controller
 * @WsController()
 */
class MessageController
{
        /**
         * 绑定用户
         * Message command is: 'message.bind'
         * @return void
         * @MessageMapping()
         */
        public function bind(): void
        {
                MessageBusiness::bind();
        }

        /**
         * 获取信息
         * Message command is: 'message.fetch'
         * @return void
         * @MessageMapping()
         */
        public function fetch(): void
        {
                MessageBusiness::fetch();
        }

        /**
         * 重置未读总数
         * Message command is: 'message.resetNotReadTotal'
         * @return void
         * @MessageMapping()
         */
        public function resetNotReadTotal(): void
        {
                MessageBusiness::resetNotReadTotal();
        }

        /**
         * 心跳检测是否保持连接
         * Message command is: 'message.heartbeat'
         * @return void
         * @MessageMapping()
         */
        public function heartbeat(): void
        {
                MessageBusiness::heartbeat();
                /*$arrParam = request()->getMessage()->toArray();
                $arrReturn = callbackParam(0, true, $arrParam);
                Session::current()->push(json_encode($arrReturn, JSON_UNESCAPED_UNICODE));*/
        }

        /**
         * Message command is: 'message.test'
         * @return void
         * @MessageMapping(middlewares={DemoMiddleware::class})
         */
        public function test(string $data): void
        {
                var_export($data);
                Session::current()->push('hi, this is message.test');
        }
}