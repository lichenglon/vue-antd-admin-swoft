<?php











namespace Composer;

use Composer\Autoload\ClassLoader;
use Composer\Semver\VersionParser;






class InstalledVersions
{
private static $installed = array (
  'root' => 
  array (
    'pretty_version' => 'dev-master',
    'version' => 'dev-master',
    'aliases' => 
    array (
    ),
    'reference' => '41fa38b9a9aa92c114f21a6aabf1e72c64bafaf3',
    'name' => 'swoft/swoft',
  ),
  'versions' => 
  array (
    'aliyuncs/oss-sdk-php' => 
    array (
      'pretty_version' => 'v2.4.2',
      'version' => '2.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '0c9d902c33847c07efc66c4cdf823deaea8fc2b6',
    ),
    'doctrine/annotations' => 
    array (
      'pretty_version' => '1.12.1',
      'version' => '1.12.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b17c5014ef81d212ac539f07a1001832df1b6d3b',
    ),
    'doctrine/instantiator' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd56bf6102915de5702778fe20f2de3b2fe570b5b',
    ),
    'doctrine/lexer' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e864bbf5904cb8f5bb334f99209b48018522f042',
    ),
    'ezyang/htmlpurifier' => 
    array (
      'pretty_version' => 'v4.13.0',
      'version' => '4.13.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '08e27c97e4c6ed02f37c5b2b20488046c8d90d75',
    ),
    'guzzlehttp/psr7' => 
    array (
      'pretty_version' => '1.8.2',
      'version' => '1.8.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dc960a912984efb74d0a90222870c72c87f10c91',
    ),
    'intervention/image' => 
    array (
      'pretty_version' => '2.5.1',
      'version' => '2.5.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'abbf18d5ab8367f96b3205ca3c89fb2fa598c69e',
    ),
    'maennchen/zipstream-php' => 
    array (
      'pretty_version' => '2.1.0',
      'version' => '2.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c4c5803cc1f93df3d2448478ef79394a5981cc58',
    ),
    'markbaker/complex' => 
    array (
      'pretty_version' => '2.0.3',
      'version' => '2.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '6f724d7e04606fd8adaa4e3bb381c3e9db09c946',
    ),
    'markbaker/matrix' => 
    array (
      'pretty_version' => '2.1.3',
      'version' => '2.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '174395a901b5ba0925f1d790fa91bab531074b61',
    ),
    'monolog/monolog' => 
    array (
      'pretty_version' => '1.26.0',
      'version' => '1.26.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '2209ddd84e7ef1256b7af205d0717fb62cfc9c33',
    ),
    'myclabs/deep-copy' => 
    array (
      'pretty_version' => '1.10.2',
      'version' => '1.10.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '776f831124e9c62e1a2c601ecc52e776d8bb7220',
      'replaced' => 
      array (
        0 => '1.10.2',
      ),
    ),
    'myclabs/php-enum' => 
    array (
      'pretty_version' => '1.8.1',
      'version' => '1.8.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '55555d31a622b4bc9662664132a0533ae6ef47b1',
    ),
    'nikic/php-parser' => 
    array (
      'pretty_version' => 'v4.10.5',
      'version' => '4.10.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '4432ba399e47c66624bc73c8c0f811e5c109576f',
    ),
    'phar-io/manifest' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '85265efd3af7ba3ca4b2a2c34dbfc5788dd29133',
    ),
    'phar-io/version' => 
    array (
      'pretty_version' => '3.1.0',
      'version' => '3.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bae7c545bef187884426f042434e561ab1ddb182',
    ),
    'php-di/phpdoc-reader' => 
    array (
      'pretty_version' => '2.2.1',
      'version' => '2.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '66daff34cbd2627740ffec9469ffbac9f8c8185c',
    ),
    'phpdocumentor/reflection-common' => 
    array (
      'pretty_version' => '2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1d01c49d4ed62f25aa84a747ad35d5a16924662b',
    ),
    'phpdocumentor/reflection-docblock' => 
    array (
      'pretty_version' => '5.2.2',
      'version' => '5.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '069a785b2141f5bcf49f3e353548dc1cce6df556',
    ),
    'phpdocumentor/type-resolver' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6a467b8989322d92aa1c8bf2bebcc6e5c2ba55c0',
    ),
    'phpoffice/phpspreadsheet' => 
    array (
      'pretty_version' => '1.18.0',
      'version' => '1.18.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '418cd304e8e6b417ea79c3b29126a25dc4b1170c',
    ),
    'phpoption/phpoption' => 
    array (
      'pretty_version' => '1.7.5',
      'version' => '1.7.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '994ecccd8f3283ecf5ac33254543eb0ac946d525',
    ),
    'phpspec/prophecy' => 
    array (
      'pretty_version' => '1.13.0',
      'version' => '1.13.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'be1996ed8adc35c3fd795488a653f4b518be70ea',
    ),
    'phpunit/php-code-coverage' => 
    array (
      'pretty_version' => '7.0.14',
      'version' => '7.0.14.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bb7c9a210c72e4709cdde67f8b7362f672f2225c',
    ),
    'phpunit/php-file-iterator' => 
    array (
      'pretty_version' => '2.0.3',
      'version' => '2.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '4b49fb70f067272b659ef0174ff9ca40fdaa6357',
    ),
    'phpunit/php-text-template' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '31f8b717e51d9a2afca6c9f046f5d69fc27c8686',
    ),
    'phpunit/php-timer' => 
    array (
      'pretty_version' => '2.1.3',
      'version' => '2.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '2454ae1765516d20c4ffe103d85a58a9a3bd5662',
    ),
    'phpunit/php-token-stream' => 
    array (
      'pretty_version' => '4.0.4',
      'version' => '4.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a853a0e183b9db7eed023d7933a858fa1c8d25a3',
    ),
    'phpunit/phpunit' => 
    array (
      'pretty_version' => '8.5.15',
      'version' => '8.5.15.0',
      'aliases' => 
      array (
      ),
      'reference' => '038d4196d8e8cb405cd5e82cedfe413ad6eef9ef',
    ),
    'psr/cache' => 
    array (
      'pretty_version' => '2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '213f9dbc5b9bfbc4f8db86d2838dc968752ce13b',
    ),
    'psr/cache-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0|2.0',
      ),
    ),
    'psr/container' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '8622567409010282b7aeebe4bb841fe98b58dcaf',
    ),
    'psr/http-client' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '2dfb5f6c5eff0e91e20e913f8c5452ed95b86621',
    ),
    'psr/http-factory' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '12ac7fcd07e5b077433f5f2bee95b3a771bf61be',
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
    ),
    'psr/http-message-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-server-handler' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'aff2f80e33b7f026ec96bb42f63242dc50ffcae7',
    ),
    'psr/http-server-middleware' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '2296f45510945530b9dceb8bcedb5cb84d40c5f5',
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.4',
      'version' => '1.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd49695b909c3b7628b6289db5479a1c204601f11',
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0.0',
      ),
    ),
    'psr/simple-cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
    ),
    'psr/simple-cache-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'ralouphie/getallheaders' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '120b605dfeb996808c31b6477290a714d356e822',
    ),
    'reasno/swoole-aliyunoss-addon' => 
    array (
      'pretty_version' => 'v1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '518575e697302842105d1774b219ebed17df345f',
    ),
    'sebastian/code-unit-reverse-lookup' => 
    array (
      'pretty_version' => '1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '1de8cd5c010cb153fcd68b8d0f64606f523f7619',
    ),
    'sebastian/comparator' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '1071dfcef776a57013124ff35e1fc41ccd294758',
    ),
    'sebastian/diff' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '14f72dd46eaf2f2293cbe79c93cc0bc43161a211',
    ),
    'sebastian/environment' => 
    array (
      'pretty_version' => '4.2.4',
      'version' => '4.2.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd47bbbad83711771f167c72d4e3f25f7fcc1f8b0',
    ),
    'sebastian/exporter' => 
    array (
      'pretty_version' => '3.1.3',
      'version' => '3.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '6b853149eab67d4da22291d36f5b0631c0fd856e',
    ),
    'sebastian/global-state' => 
    array (
      'pretty_version' => '3.0.1',
      'version' => '3.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '474fb9edb7ab891665d3bfc6317f42a0a150454b',
    ),
    'sebastian/object-enumerator' => 
    array (
      'pretty_version' => '3.0.4',
      'version' => '3.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e67f6d32ebd0c749cf9d1dbd9f226c727043cdf2',
    ),
    'sebastian/object-reflector' => 
    array (
      'pretty_version' => '1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '9b8772b9cbd456ab45d4a598d2dd1a1bced6363d',
    ),
    'sebastian/recursion-context' => 
    array (
      'pretty_version' => '3.0.1',
      'version' => '3.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '367dcba38d6e1977be014dc4b22f47a484dac7fb',
    ),
    'sebastian/resource-operations' => 
    array (
      'pretty_version' => '2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '31d35ca87926450c44eae7e2611d45a7a65ea8b3',
    ),
    'sebastian/type' => 
    array (
      'pretty_version' => '1.1.4',
      'version' => '1.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '0150cfbc4495ed2df3872fb31b26781e4e077eb4',
    ),
    'sebastian/version' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '99732be0ddb3361e16ad77b68ba41efc8e979019',
    ),
    'swoft/annotation' => 
    array (
      'pretty_version' => 'v2.0.11',
      'version' => '2.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '82a988feb4bcc78fe5ac64abd845bc766e986ea5',
    ),
    'swoft/aop' => 
    array (
      'pretty_version' => 'v2.0.11',
      'version' => '2.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '0719114d4b6fd36345bdb49c84e6f16990b619be',
    ),
    'swoft/apollo' => 
    array (
      'pretty_version' => 'v2.0.11',
      'version' => '2.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f3e77a307f684182bcf94eac197b32ade20a5a34',
    ),
    'swoft/bean' => 
    array (
      'pretty_version' => 'v2.0.11',
      'version' => '2.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '9211987977d0907bbc4a8b3c183087b01543e36d',
    ),
    'swoft/breaker' => 
    array (
      'pretty_version' => 'v2.0.11',
      'version' => '2.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '8c446deeaa938439ae0d9d0434a07bb39281f56e',
    ),
    'swoft/cache' => 
    array (
      'pretty_version' => 'v2.0.3',
      'version' => '2.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c737c9e4d9313290c8cfcc1cb2ddfa2441bec2c0',
    ),
    'swoft/config' => 
    array (
      'pretty_version' => 'v2.0.11',
      'version' => '2.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f2d2b66105efb688fb15bbd23e2b96dc1b4bc183',
    ),
    'swoft/connection-pool' => 
    array (
      'pretty_version' => 'v2.0.11',
      'version' => '2.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a7d27f370e22e4d5bd18c8f8bc9991f9eb082fea',
    ),
    'swoft/console' => 
    array (
      'pretty_version' => 'v2.0.11',
      'version' => '2.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '51e3b10177e4b72b952b1d12d98cc9eca6a49b06',
    ),
    'swoft/consul' => 
    array (
      'pretty_version' => 'v2.0.11',
      'version' => '2.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '01ece196181612e6d282feeca47f87fe2193096f',
    ),
    'swoft/crontab' => 
    array (
      'pretty_version' => 'v2.0.11',
      'version' => '2.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '27ed1af3e57e9f3d58ad0e9bfecfd83bb1fb2e98',
    ),
    'swoft/db' => 
    array (
      'pretty_version' => 'v2.0.11',
      'version' => '2.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '9105b2721d16b76c1487ccbcc2d1c9eb2ca32113',
    ),
    'swoft/devtool' => 
    array (
      'pretty_version' => 'v2.0.11',
      'version' => '2.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ad38428d18e9fbbc46348c61e06d1cf2a9c1d82d',
    ),
    'swoft/error' => 
    array (
      'pretty_version' => 'v2.0.11',
      'version' => '2.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'be034a90f86464991f1ce46e7a1d0826983fbd95',
    ),
    'swoft/event' => 
    array (
      'pretty_version' => 'v2.0.11',
      'version' => '2.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '8dedb9fa3f9e25ddcfee9b7cd98586b0255586fa',
    ),
    'swoft/framework' => 
    array (
      'pretty_version' => 'v2.0.11',
      'version' => '2.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b5c85add680c91d3f8784ada3b2c5295944e50e1',
    ),
    'swoft/http-message' => 
    array (
      'pretty_version' => 'v2.0.11',
      'version' => '2.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '38cca173917b9a42026088dcfb68218c93847148',
    ),
    'swoft/http-server' => 
    array (
      'pretty_version' => 'v2.0.11',
      'version' => '2.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '411b63432cfbc284933ac58ee5f3ee7fe6c1ea8a',
    ),
    'swoft/i18n' => 
    array (
      'pretty_version' => 'v2.0.11',
      'version' => '2.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '8b803392caeb96306201466567b6b6fb1425819d',
    ),
    'swoft/limiter' => 
    array (
      'pretty_version' => 'v2.0.11',
      'version' => '2.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '8ccfdbcd8fa0929eea132c6991c521cf80ce2dda',
    ),
    'swoft/log' => 
    array (
      'pretty_version' => 'v2.0.11',
      'version' => '2.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ef60c53b40783e75c9620bd2ec76aef0ee4d8a11',
    ),
    'swoft/process' => 
    array (
      'pretty_version' => 'v2.0.11',
      'version' => '2.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '1d57af07bdb46f56e6372c031d3b521e645d46bf',
    ),
    'swoft/proxy' => 
    array (
      'pretty_version' => 'v2.0.11',
      'version' => '2.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '06de916020cb3c620aebc1e72a1f7d23fddc3cdd',
    ),
    'swoft/redis' => 
    array (
      'pretty_version' => 'v2.0.11',
      'version' => '2.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '768bb0643e1db6774399a0621c4ec05e55fa7895',
    ),
    'swoft/rpc' => 
    array (
      'pretty_version' => 'v2.0.11',
      'version' => '2.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ced4034d6fa93b26f8026325935a388b553a4709',
    ),
    'swoft/rpc-client' => 
    array (
      'pretty_version' => 'v2.0.11',
      'version' => '2.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '94dbb36b91697bf3fb63038a77e971ea5d39a9d6',
    ),
    'swoft/rpc-server' => 
    array (
      'pretty_version' => 'v2.0.11',
      'version' => '2.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '9e08b4ea88f4b139cb26106303a594ebf6f21a6f',
    ),
    'swoft/serialize' => 
    array (
      'pretty_version' => 'v2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '58fd2f1f3a867bac64aa07483cc00ade2af9dd19',
    ),
    'swoft/server' => 
    array (
      'pretty_version' => 'v2.0.11',
      'version' => '2.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '2809bce67518756d4b144a3e0f181a3f764face9',
    ),
    'swoft/session' => 
    array (
      'pretty_version' => 'v2.0.11',
      'version' => '2.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'af9a33d6a9adefdea5eeda3ba4acc080f6a40b59',
    ),
    'swoft/stdlib' => 
    array (
      'pretty_version' => 'v2.0.11',
      'version' => '2.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '9609105a54388515c84b408e4abb5171db25292b',
    ),
    'swoft/swlib' => 
    array (
      'pretty_version' => 'v2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '904ffaf78a263e1842564dc6d6d7712ae59b583f',
    ),
    'swoft/swoft' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => '41fa38b9a9aa92c114f21a6aabf1e72c64bafaf3',
    ),
    'swoft/swoole-ide-helper' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
        0 => '9999999-dev',
      ),
      'reference' => 'cebe5253a3ca152c49545bc08514ac28103b6d78',
    ),
    'swoft/task' => 
    array (
      'pretty_version' => 'v2.0.11',
      'version' => '2.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '8f700d0321379e195c76865bfd985454e823edd7',
    ),
    'swoft/tcp' => 
    array (
      'pretty_version' => 'v2.0.11',
      'version' => '2.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'eeb66e1357331ad1eb8872ebbc29a26a9dd4ce6c',
    ),
    'swoft/tcp-server' => 
    array (
      'pretty_version' => 'v2.0.11',
      'version' => '2.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '8e72620fc15c0244a48ff47c53a10e693c3771ff',
    ),
    'swoft/validator' => 
    array (
      'pretty_version' => 'v2.0.11',
      'version' => '2.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '80052ece3b637d2f7781b07bcd0fd1c440cddfb2',
    ),
    'swoft/view' => 
    array (
      'pretty_version' => 'v2.0.11',
      'version' => '2.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a30a97bcf9f5867828dfd676a5dc4f2fe572d354',
    ),
    'swoft/websocket-server' => 
    array (
      'pretty_version' => 'v2.0.11',
      'version' => '2.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '6bb96d76507a9fc05fcc2cbc0a633f8047c3c12e',
    ),
    'symfony/cache' => 
    array (
      'pretty_version' => 'v5.2.7',
      'version' => '5.2.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '1d801d1dc5e3840e832568db6b35a954cfb435a8',
    ),
    'symfony/cache-contracts' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c0446463729b89dd4fa62e9aeecc80287323615d',
    ),
    'symfony/cache-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0|2.0',
      ),
    ),
    'symfony/expression-language' => 
    array (
      'pretty_version' => 'v4.4.22',
      'version' => '4.4.22.0',
      'aliases' => 
      array (
      ),
      'reference' => '0dd911bbb99d7210a8f38d8de4a7964ff4a06533',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'pretty_version' => 'v1.22.1',
      'version' => '1.22.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c6c942b1ac76c82448322025e084cadc56048b4e',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '2df51500adbaebdc4c38dea4c89a2e131c45c8a1',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.22.1',
      'version' => '1.22.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dc3063ba22c2a1fd2f45ed856374d79114998f91',
    ),
    'symfony/service-contracts' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f040a30e04b57fbcc9c6cbcf4dbaa96bd318b9bb',
    ),
    'symfony/var-exporter' => 
    array (
      'pretty_version' => 'v5.2.7',
      'version' => '5.2.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '01184a5ab95eb9500b9b0ef3e525979e003d9c81',
    ),
    'symfony/yaml' => 
    array (
      'pretty_version' => 'v4.4.22',
      'version' => '4.4.22.0',
      'aliases' => 
      array (
      ),
      'reference' => '1c2fd24147961525eaefb65b11987cab75adab59',
    ),
    'text/template' => 
    array (
      'pretty_version' => 'v2.6.3',
      'version' => '2.6.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fd77c2aeb9c749426c7f6b5f7ab93c8de90a7cbc',
    ),
    'theseer/tokenizer' => 
    array (
      'pretty_version' => '1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '75a63c33a8577608444246075ea0af0d052e452a',
    ),
    'toolkit/cli-utils' => 
    array (
      'pretty_version' => 'v1.2.4',
      'version' => '1.2.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b7b94b43e9e3af10e0203c6abb115309b6cb73ce',
    ),
    'vlucas/phpdotenv' => 
    array (
      'pretty_version' => 'v3.6.8',
      'version' => '3.6.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '5e679f7616db829358341e2d5cccbd18773bdab8',
    ),
    'webmozart/assert' => 
    array (
      'pretty_version' => '1.10.0',
      'version' => '1.10.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6964c76c7804814a842473e0c8fd15bab0f18e25',
    ),
  ),
);
private static $canGetVendors;
private static $installedByVendor = array();







public static function getInstalledPackages()
{
$packages = array();
foreach (self::getInstalled() as $installed) {
$packages[] = array_keys($installed['versions']);
}


if (1 === \count($packages)) {
return $packages[0];
}

return array_keys(array_flip(\call_user_func_array('array_merge', $packages)));
}









public static function isInstalled($packageName)
{
foreach (self::getInstalled() as $installed) {
if (isset($installed['versions'][$packageName])) {
return true;
}
}

return false;
}














public static function satisfies(VersionParser $parser, $packageName, $constraint)
{
$constraint = $parser->parseConstraints($constraint);
$provided = $parser->parseConstraints(self::getVersionRanges($packageName));

return $provided->matches($constraint);
}










public static function getVersionRanges($packageName)
{
foreach (self::getInstalled() as $installed) {
if (!isset($installed['versions'][$packageName])) {
continue;
}

$ranges = array();
if (isset($installed['versions'][$packageName]['pretty_version'])) {
$ranges[] = $installed['versions'][$packageName]['pretty_version'];
}
if (array_key_exists('aliases', $installed['versions'][$packageName])) {
$ranges = array_merge($ranges, $installed['versions'][$packageName]['aliases']);
}
if (array_key_exists('replaced', $installed['versions'][$packageName])) {
$ranges = array_merge($ranges, $installed['versions'][$packageName]['replaced']);
}
if (array_key_exists('provided', $installed['versions'][$packageName])) {
$ranges = array_merge($ranges, $installed['versions'][$packageName]['provided']);
}

return implode(' || ', $ranges);
}

throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}





public static function getVersion($packageName)
{
foreach (self::getInstalled() as $installed) {
if (!isset($installed['versions'][$packageName])) {
continue;
}

if (!isset($installed['versions'][$packageName]['version'])) {
return null;
}

return $installed['versions'][$packageName]['version'];
}

throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}





public static function getPrettyVersion($packageName)
{
foreach (self::getInstalled() as $installed) {
if (!isset($installed['versions'][$packageName])) {
continue;
}

if (!isset($installed['versions'][$packageName]['pretty_version'])) {
return null;
}

return $installed['versions'][$packageName]['pretty_version'];
}

throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}





public static function getReference($packageName)
{
foreach (self::getInstalled() as $installed) {
if (!isset($installed['versions'][$packageName])) {
continue;
}

if (!isset($installed['versions'][$packageName]['reference'])) {
return null;
}

return $installed['versions'][$packageName]['reference'];
}

throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}





public static function getRootPackage()
{
$installed = self::getInstalled();

return $installed[0]['root'];
}







public static function getRawData()
{
return self::$installed;
}



















public static function reload($data)
{
self::$installed = $data;
self::$installedByVendor = array();
}




private static function getInstalled()
{
if (null === self::$canGetVendors) {
self::$canGetVendors = method_exists('Composer\Autoload\ClassLoader', 'getRegisteredLoaders');
}

$installed = array();

if (self::$canGetVendors) {
foreach (ClassLoader::getRegisteredLoaders() as $vendorDir => $loader) {
if (isset(self::$installedByVendor[$vendorDir])) {
$installed[] = self::$installedByVendor[$vendorDir];
} elseif (is_file($vendorDir.'/composer/installed.php')) {
$installed[] = self::$installedByVendor[$vendorDir] = require $vendorDir.'/composer/installed.php';
}
}
}

$installed[] = self::$installed;

return $installed;
}
}
