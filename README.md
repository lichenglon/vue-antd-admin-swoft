# vue-antd-admin+swoft权限框架
![输入图片说明](swoft/public/uploads/20210517/image.png)
![输入图片说明](swoft/public/uploads/20210517/02.jpgimage.png)
![输入图片说明](swoft/public/uploads/20210517/03image.png)
![输入图片说明](swoft/public/uploads/20210517/04image.png)
![输入图片说明](swoft/public/uploads/20210517/05image.png)
![输入图片说明](swoft/public/uploads/20210517/06image.png)
![输入图片说明](swoft/public/uploads/20210517/07image.png)

#### 介绍
一款基于vue+swoole的后台权限框架支持异步秒级定时器多进程的服务框架

#### 软件架构
vue-antd-admin + swoft2.0


#### 安装教程

1.  下载对应你系统的Node.js版本:https://nodejs.org/en/download/

2.  用npm安装yarn

在一些项目中yarn的表现更出色一些，所以我们选择用刚安装好的npm来安装yarn

安装命令：

npm install -g yarn --registry=https://registry.npm.taobao.org
配置源

yarn config set registry https://registry.npm.taobao.org -g
 
yarn config set sass_binary_site http://cdn.npm.taobao.org/dist/node-sass -g
执行yarn -version如果可以显示版本则证明安装成功。
3. 进入vue-antd-admin目录执行yarn install 初始化项目初始化完成后

4. 执行 yarn serve
文档地址https://iczer.gitee.io/vue-antd-admin-docs/start/use.html

#### 使用说明

1. 演示地址http://long.admin.wyumi.cn 账号：demo 密码：123456
如有意向可私信我

#### 参与贡献

1.  Fork 本仓库


#### 特技

1.高级表格支持动态添加搜索表头用户自定义

2.水印无法去除为销售系统的量身定做

3.已实现长连接消息通知

4.restful API接口规范